﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/05>
    /// Description: <مناسبت تقویم مشتری>
    /// </summary>
    public partial class CustomCalendarDetailPage : ItcBaseControl

    {
        #region PublicParam:
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private readonly CalendarTypeBL _calendarTypeBL = new CalendarTypeBL();
        private readonly CustomCalendarDetailBL _customCalendarDetailBL = new CustomCalendarDetailBL();
        private const string TableName = "Calendar.V_CustomCalendarDetail";
        private const string PrimaryKey = "CustomCalendarDetailId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            
            cmbOccasionDayTitle.ClearSelection();
            cmbOccasionDayType.ClearSelection();
            cmbCalendarType.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var customCalendarDetailEntity = new CustomCalendarDetailEntity()
            {
                CustomCalendarDetailId = int.Parse(ViewState["CustomCalendarDetailId"].ToString())
            };
            var mycustomCalendarDetail = _customCalendarDetailBL.GetSingleById(customCalendarDetailEntity);
            
            if (cmbCalendarType.FindItemByValue(mycustomCalendarDetail.CalendarTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع تقویم انتخاب شده معتبر نمی باشد.");
            }

            cmbCalendarType.SelectedValue = mycustomCalendarDetail.CalendarTypeId.ToString();
            if (cmbOccasionDayType.FindItemByValue(mycustomCalendarDetail.OccasionDayTypeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد نوع مناسبت انتخاب شده معتبر نمی باشد.");
            }

            cmbOccasionDayType.SelectedValue = mycustomCalendarDetail.OccasionDayTypeId.ToString();

            SetComboBoxOccasionDay(mycustomCalendarDetail.OccasionDayTypeId);
       

            cmbOccasionDayTitle.SelectedValue = mycustomCalendarDetail.OccasionDayId.ToString();
            cmbIsActive.SelectedValue = (mycustomCalendarDetail.IsActive.ToString());
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdCustomCalendarDetail,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbCalendarType.Items.Clear();
            cmbCalendarType.Items.Add(new RadComboBoxItem(""));
            cmbCalendarType.DataSource = _calendarTypeBL.GetAllIsActive();
            cmbCalendarType.DataTextField = "CalendarTypePersianTitle";
            cmbCalendarType.DataValueField = "CalendarTypeId";
            cmbCalendarType.DataBind();

            cmbOccasionDayType.Items.Clear();
            cmbOccasionDayType.Items.Add(new RadComboBoxItem(""));
            cmbOccasionDayType.DataSource = _occasionDayTypeBL.GetAllIsActive();
            cmbOccasionDayType.DataTextField = "OccasionDayTypeTitle";
            cmbOccasionDayType.DataValueField = "OccasionDayTypeId";
            cmbOccasionDayType.DataBind();

        }

        private void SetComboBoxOccasionDay(int OccasionDayTypeId)
        {
            cmbOccasionDayTitle.Items.Clear();
            cmbOccasionDayTitle.Items.Add(new RadComboBoxItem(""));
            OccasionDayEntity occasionDayEntity=new OccasionDayEntity()
            {
                OccasionDayTypeId = OccasionDayTypeId
            };
            cmbOccasionDayTitle.DataSource = _occasionDayBL.GetOccasionDayCollectionByOccasionDayType(occasionDayEntity);
            cmbOccasionDayTitle.DataTextField = "OccasionDayTitle";
            cmbOccasionDayTitle.DataValueField = "OccasionDayId";
            cmbOccasionDayTitle.DataBind();
            
        }

        #endregion

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "CustomCalendarId ='" + Session["CustomCalendarId"] + "'";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int CustomCalendarDetailId;
                ViewState["WhereClause"] = "CustomCalendarId ='" + Session["CustomCalendarId"] + "'";
                SetPanelFirst();
                var customCalendarDetailEntity = new CustomCalendarDetailEntity()
                {
                    CalendarTypeId = int.Parse(cmbCalendarType.SelectedValue),
                    OccasionDayId = int.Parse(cmbOccasionDayTitle.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    CustomCalendarId = int.Parse(Session["CustomCalendarId"].ToString()),
                    
                };

                _customCalendarDetailBL.Add(customCalendarDetailEntity, out CustomCalendarDetailId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, CustomCalendarDetailId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "CustomCalendarId ='" + Session["CustomCalendarId"] + "'";
                

                if (cmbCalendarType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and CalendarTypeId= '" +
                                               int.Parse(cmbCalendarType.SelectedValue) + "'";
                if (cmbOccasionDayType.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeId= '" +
                                               int.Parse(cmbOccasionDayType.SelectedValue) + "'";

                if (cmbOccasionDayTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId= '" +
                                               int.Parse(cmbOccasionDayTitle.SelectedValue) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdCustomCalendarDetail.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdCustomCalendarDetail.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "CustomCalendarId ='" + Session["CustomCalendarId"] + "'";
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var customCalendarDetailEntity = new CustomCalendarDetailEntity()
                {
                    CustomCalendarDetailId = int.Parse(ViewState["CustomCalendarDetailId"].ToString()),
                    CalendarTypeId = int.Parse(cmbCalendarType.SelectedValue),
                    OccasionDayId = int.Parse(cmbOccasionDayTitle.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    CustomCalendarId = int.Parse(Session["CustomCalendarId"].ToString()),

                };
                _customCalendarDetailBL.Update(customCalendarDetailEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, int.Parse(ViewState["CustomCalendarDetailId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdCustomCalendarDetail_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["CustomCalendarDetailId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "CustomCalendarId ='" + Session["CustomCalendarId"] + "'";
                    var customCalendarDetailEntity = new CustomCalendarDetailEntity()
                    {
                        CustomCalendarDetailId = int.Parse(e.CommandArgument.ToString()),
                    };
                    _customCalendarDetailBL.Delete(customCalendarDetailEntity);
                    var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdCustomCalendarDetail_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdCustomCalendarDetail_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdCustomCalendarDetail_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdCustomCalendarDetail.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
        protected void cmbOccasionDayType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(cmbOccasionDayType.SelectedValue))
            {
                SetComboBoxOccasionDay(int.Parse(e.Value));

            }
            else
            {
                cmbOccasionDayTitle.Items.Clear();
                cmbOccasionDayTitle.Text = "";
            }
        }
        #endregion

        
        

    }
}