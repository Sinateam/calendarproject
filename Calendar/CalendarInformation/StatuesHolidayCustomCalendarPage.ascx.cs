﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation
{
    public partial class StatuesHolidayCustomCalendarPage : ItcBaseControl
    {
        private readonly CustomCalendarBL _customCalendarBL=new CustomCalendarBL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        #region  Procedure:

        void SetDataShow()
        {
            CustomCalendarEntity customCalendarEntity=new CustomCalendarEntity()
            {
                CustomCalendarId = int.Parse(Session["CustomCalendarId"].ToString())
            };
            CustomCalendarEntity myCustomCalendarEntity = _customCalendarBL.GetSingleById(customCalendarEntity);
            cmbIsHoliday.SelectedValue = myCustomCalendarEntity.IsHoliday.ToString();
            

        }
#endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            SetDataShow();
        }

        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var customCalendarEntity = new CustomCalendarEntity()
                {
                    CustomCalendarId = int.Parse(Session["CustomCalendarId"].ToString()),
                    IsHoliday = bool.Parse(cmbIsHoliday.SelectedItem.Value)
                };
                _customCalendarBL.UpdateIsHoliday(customCalendarEntity);                                                
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
       

    }
}