﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomCalendarPage.ascx.cs" Inherits="Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation.CustomCalendarPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<table align="right" dir="rtl" width="100%">
    <tr>
        <td>
            <asp:Panel ID="pnlGenerateDate" runat="server">
                <table align="right" dir="rtl" width="100%">
                    <tr>
                        <td colspan="5">
                            <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5" height="50">
                            <asp:Label ID="Label1" runat="server" Text="تولید تاریخ بر مبنای سازمان" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" valign="middle" nowrap="nowrap">
                            عنوان سال<font color="red">*</font>:
                        </td>
                        <td width="25%" valign="middle">
                            <cc1:CustomRadComboBox ID="cmbYear" runat="server" AppendDataBoundItems="True" Width="300px">
                            </cc1:CustomRadComboBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="cmbYear"
                                ValidationGroup="ConvertDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td nowrap="nowrap" width="5%">
                            ساختار سازمانی<font color="red">*</font>:
                        </td>
                        <td width="25%">
                            <cc1:SelectControl runat="server" PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx"
                                imageName="OrganizationPhysicalChart" ID="SelectControlOrganizationPhysicalChart">
                            </cc1:SelectControl>
                            </td>
                            <td >
                                <asp:ImageButton ID="ImageCopyDate" runat="server" ImageUrl="../Images/CopyDate.png"
                                    ValidationGroup="ConvertDate" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"
                                    Height="30px" Width="30px" OnClick="ImageCopyDate_Click" />
                                <asp:ImageButton ID="imgbtnOtherInfo" runat="server" ImageUrl="../Images/AddOtherInfo.png"
                                    BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Height="30px" Width="30px"
                                    CausesValidation="False" OnClick="imgbtnOtherInfo_Click" />
                            </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
    <tr>
        <td>
            <asp:Panel ID="pnlCopyGeneratDate" runat="server">
                <table width="100%">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="5%">
                                        <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" OnClick="btnSearch_Click">
                                            <Icon PrimaryIconCssClass="rbSearch" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td width="5%">
                                        <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" OnClick="btnShowAll_Click">
                                            <Icon PrimaryIconCssClass="rbRefresh" />
                                        </cc1:CustomRadButton>
                                    </td>
                                    <td>
                                        <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="15%">
                                        <asp:Label runat="server" ID="lblMiladiDate">تاریخ تقویم:</asp:Label>
                                    </td>
                                    <td width="85%" align="right" dir="rtl">
                                        <cc1:CustomItcCalendar ID="txtMiladiDate" runat="server"></cc1:CustomItcCalendar>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">
                                        ساختار سازمانی:
                                    </td>
                                    <td>
                                        <cc1:SelectControl runat="server" PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx"
                                            imageName="OrganizationPhysicalChart" ID="SelectControlOrganizationPhysicalChartSearch">
                                        </cc1:SelectControl>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="middle" nowrap="nowrap">
                                        عنوان سال:
                                    </td>
                                    <td valign="middle">
                                        <cc1:CustomRadComboBox ID="cmbYearSearch" runat="server" AppendDataBoundItems="True"
                                            Width="300px">
                                        </cc1:CustomRadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblMonthTitle">ماه:</asp:Label>
                                    </td>
                                    <td>
                                        <cc1:CustomRadComboBox ID="cmbMonthTitle" runat="server" AppendDataBoundItems="True">
                                        </cc1:CustomRadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblWeekDayTitle">عنوان روز:</asp:Label>
                                    </td>
                                    <td>
                                        <cc1:CustomRadComboBox ID="cmbWeekDayTitle" runat="server" AppendDataBoundItems="True">
                                        </cc1:CustomRadComboBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblDayNo"> روز:</asp:Label>
                                    </td>
                                    <td>
                                        <cc1:NumericTextBox runat="server" ID="txtDayNo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" ID="lblIsHoliday">آیا تعطیل می باشد؟:</asp:Label>
                                    </td>
                                    <td>
                                        <cc1:CustomRadComboBox ID="cmbIsHoliday" runat="server" AppendDataBoundItems="True">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Text="" Value="" />
                                                <telerik:RadComboBoxItem runat="server" Text="خیر" Value="0" />
                                                <telerik:RadComboBoxItem runat="server" Text="بله" Value="1" />
                                            </Items>
                                        </cc1:CustomRadComboBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
    <tr>
        <td>
            <telerik:RadGrid ID="grdGeneralCalendar" runat="server" AllowCustomPaging="True"
                AllowPaging="True" AllowSorting="True" ShowFooter="True" CellSpacing="0" GridLines="None"
                HorizontalAlign="Center" Skin="Office2007" AutoGenerateColumns="False" OnItemCommand="grdGeneralCalendar_ItemCommand"
                OnPageIndexChanged="grdGeneralCalendar_PageIndexChanged" OnPageSizeChanged="grdGeneralCalendar_PageSizeChanged"
                OnSortCommand="grdGeneralCalendar_SortCommand">
                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                </HeaderContextMenu>
                <MasterTableView DataKeyNames="CustomCalendarId" Dir="RTL" GroupsDefaultExpanded="False"
                    NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                    <Columns>
                        <telerik:GridBoundColumn DataField="ShamsiDate" HeaderText="تاریخ" SortExpression="ShamsiDate">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OrganizationPhysicalChartTitle" HeaderText="ساختارسازمانی" SortExpression="OrganizationPhysicalChartTitle">
                        </telerik:GridBoundColumn>

                        
                        <telerik:GridBoundColumn DataField="YearPersianTitle" FilterControlAltText="Filter YearTitle column"
                            HeaderText=" عنوان سال فارسی" SortExpression="YearPersianTitle" UniqueName="YearPersianTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="YearArabicTitle" FilterControlAltText="Filter YearTitle column"
                            HeaderText=" عنوان سال قمری" SortExpression="YearArabicTitle" UniqueName="YearArabicTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="YearEnglishTitle" FilterControlAltText="Filter YearTitle column"
                            HeaderText=" عنوان سال میلادی" SortExpression="YearEnglishTitle" UniqueName="YearEnglishTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MonthPersianTitle" FilterControlAltText="Filter MonthTitle column"
                            HeaderText="عنوان ماه فارسی" SortExpression="MonthPersianTitle" UniqueName="MonthPersianTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MonthArabicTitle" FilterControlAltText="Filter MonthTitle column"
                            HeaderText="عنوان ماه قمری" SortExpression="MonthArabicTitle" UniqueName="MonthArabicTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="MonthEnglishTitle" FilterControlAltText="Filter MonthTitle column"
                            HeaderText="عنوان ماه میلادی" SortExpression="MonthEnglishTitle" UniqueName="MonthEnglishTitle">
                        </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PersianDayNo" HeaderText="روز فارسی" SortExpression="PersianDayNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ArabicDayNo" HeaderText="روز قمری" SortExpression="ArabicDayNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EnglishDayNo" HeaderText="روز میلادی" SortExpression="EnglishDayNo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="WeekDayPersianTitle" FilterControlAltText="Filter WeekDayTitle column"
                            HeaderText="عنوان روز هفته" SortExpression="WeekDayPersianTitle" UniqueName="WeekDayPersianTitle">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsHoliday" HeaderText="وضیعت تعطیلات" SortExpression="IsHoliday">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="اطلاعات تکمیلی"
                            UniqueName="Add">
                            <ItemTemplate>
                                <asp:ImageButton ID="ImgButton3" runat="server" CausesValidation="false" CommandArgument='<%#Eval("CustomCalendarId").ToString() %>'
                                    CommandName="OtherInformation" ForeColor="#000066" ImageUrl="../Images/AddOtherInfo.png" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه" 
                            UniqueName="_ShowAll">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("CustomCalendarId").ToString() %>'
                                    CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../Images/ShowAllInGrid.png" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                    </ExpandCollapseColumn>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                    <PagerStyle AlwaysVisible="True" FirstPageToolTip="صفحه اول" HorizontalAlign="Center"
                        LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی"
                        PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                        PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                        VerticalAlign="Middle" />
                </MasterTableView>
            </telerik:RadGrid>
            <br />
        </td>
    </tr>
    </table> </asp:Panel> </td> </tr>
    <tr>
        <td>
            <asp:Panel ID="pnlCopyGeneratDateDetail" runat="server" 
                 HorizontalAlign="Right" Width="100%">
                <table width="100%" dir="rtl">
                    <tr>
                        <td align="right">
                            <telerik:RadTabStrip ID="radtabCalender" runat="server" Skin="Windows7"
                                Align="Right" AutoPostBack="True" CausesValidation="False" OnTabClick="radtabCalender_TabClick">
                                <Tabs>
                                    <telerik:RadTab runat="server" Text="وضیعت تعطیلی" PageViewID="CalendarInformation\StatuesHolidayCustomCalendarPage" Value="StatuesHolidayCustomCalendarPage">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="مناسبت ها" Value="CustomCalendarDetailPage" PageViewID="CalendarInformation\CustomCalendarDetailPage">
                                    </telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadMultiPage ID="radmultipageCalander" runat="server" Height="100%" 
                                Width="100%" onpageviewcreated="radmultipageCalander_PageViewCreated">
                            </telerik:RadMultiPage>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </td>
    </tr>
</table>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="cmbCalendarType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbCalendarType" />
                <telerik:AjaxUpdatedControl ControlID="cmbYear" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnAddDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnDeleteDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ImageCopyDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnOtherInfo">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdGeneralCalendar">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDateDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
