﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GeneralCalendarPage.ascx.cs"
    Inherits="Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation.GeneralCalendarPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="ITC.Library" Namespace="ITC.Library.Controls" TagPrefix="cc1" %>
<table align="right" dir="rtl" width="100%">
    <tr>
        <td>
            <asp:Panel ID="pnlGenerateDate" runat="server">
                <table align="right" dir="rtl" width="100%">
                    <tr>
                        <td colspan="5">
                            <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" height="50">
                            <asp:Label ID="lblTitle" runat="server" Text="تولید تاریخ" Font-Bold="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%" hieght="30" height="30" align="right" valign="middle" nowrap="nowrap">
                            نوع تقویم<font color="red">*</font>:
                        </td>
                        <td width="30%" valign="middle">
                            <cc1:CustomRadComboBox ID="cmbCalendarType" runat="server" AppendDataBoundItems="True"
                                AutoPostBack="True" CausesValidation="False" OnSelectedIndexChanged="cmbCalendarType_SelectedIndexChanged"
                                Width="300px">
                            </cc1:CustomRadComboBox>
                            <asp:RequiredFieldValidator ID="rfvCalendarType" runat="server" ControlToValidate="cmbCalendarType"
                                ValidationGroup="ConvertDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td width="5%" valign="middle" nowrap="nowrap">
                            عنوان سال<font color="red">*</font>:
                        </td>
                        <td width="30%" valign="middle">
                            <cc1:CustomRadComboBox ID="cmbYear" runat="server" AppendDataBoundItems="True" Width="300px">
                            </cc1:CustomRadComboBox>
                            <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="cmbYear"
                                ValidationGroup="ConvertDate" ErrorMessage="*"></asp:RequiredFieldValidator>
                        </td>
                        <td align="right" valign="middle">
                            <asp:ImageButton ID="imgbtnAddDate" runat="server" ImageUrl="../Images/AddDate.png"
                                ValidationGroup="ConvertDate" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"
                                Height="30px" OnClick="imgbtnAddDate_Click" Width="30px" />
                            <asp:ImageButton ID="imgbtnDeleteDate" runat="server" ImageUrl="../Images/DeleteDate.png"
                                ValidationGroup="ConvertDate" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"
                                Height="30px" Width="30px" OnClick="imgbtnDeleteDate_Click" />
                        </td>
                    </tr>
    
                </table>
            </asp:Panel>
        </td>
    </tr>

</table>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="cmbCalendarType">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="cmbCalendarType" />
                <telerik:AjaxUpdatedControl ControlID="cmbYear" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnAddDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnDeleteDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="ImageCopyDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="imgbtnOtherInfo">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdGeneralCalendar">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGenerateDate" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDate" LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="pnlCopyGeneratDateDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
</telerik:RadAjaxLoadingPanel>
