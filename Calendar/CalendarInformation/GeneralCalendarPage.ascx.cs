﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <مشخصات ماه های سال>
    /// </summary>

    public partial class GeneralCalendarPage : ItcBaseControl
    {

        #region PublicParam:                
        private readonly YearBL _yearBL = new YearBL();
        private readonly CalendarTypeBL _calendarTypeBL = new CalendarTypeBL();
        private readonly GeneralCalendarBL _generalCalendarBL = new GeneralCalendarBL();
        

        #endregion
        #region Procedure:

        

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            
            cmbCalendarType.ClearSelection();
            cmbYear.ClearSelection();
        

        }







        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }




        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbCalendarType.Items.Clear();
            cmbCalendarType.Items.Add(new RadComboBoxItem(""));
            cmbCalendarType.DataSource = _calendarTypeBL.GetAllIsActive();
            cmbCalendarType.DataTextField = "CalendarTypePersianTitle";
            cmbCalendarType.DataValueField = "CalendarTypeId";
            cmbCalendarType.DataBind();



        }




        private void SetCmbYear(int calendarTypeId)
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add(new RadComboBoxItem(""));
            var yearEntity = new YearEntity()
            {
                CalendarTypeId = calendarTypeId
            };
            cmbYear.DataSource = _yearBL.GetYearCollectionByCalendarType(yearEntity);
            cmbYear.DataTextField = "YearTitle";
            cmbYear.DataValueField = "YearId";
            cmbYear.DataBind();
        }


        private void AddPageView(RadTab tab)
        {
            var pageView = new RadPageView();
            pageView.ID = tab.PageViewID;
            

        }
        #endregion

        #region ControlEvent:
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                SetClearToolBox();
                SetComboBox();
                
            }
            catch (Exception ex)
            {

               
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        protected void imgbtnAddDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {                
                ViewState["WhereClause"] = "";
                var generationDateEntity = new GenerationDateEntity()
                {
                    YearId= int.Parse(cmbYear.SelectedValue),
                    CalendarTypeId = int.Parse(cmbCalendarType.SelectedValue)
                };
                _generalCalendarBL.GenerationDate(generationDateEntity);
                SetClearToolBox();                
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }


     

        protected void imgbtnDeleteDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "";
                var generationDateEntity = new GenerationDateEntity()
                {
                    YearId = int.Parse(cmbYear.SelectedValue),
                    CalendarTypeId = int.Parse(cmbCalendarType.SelectedValue)
                };
                _generalCalendarBL.GenerationDateDelete(generationDateEntity);
                SetClearToolBox();
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void cmbCalendarType_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(e.Value))
            {
                SetCmbYear(int.Parse(e.Value));
            }
            else
            {
                cmbYear.Items.Clear();
                cmbYear.Text = "";
            }
        }

   

       
        

        
    }
}