﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StructureCustomCalendarPage.ascx.cs" Inherits="Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation.StructureCustomCalendarPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<asp:Panel ID="pnlGeneral" runat="server" Width="100%">
<table align="right" dir="rtl" width="100%">
    <tr>
        <td colspan="5">
     
            <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl1" runat="server" />
     
        </td>
    </tr>
    <tr>
        <td width="10%">
            عنوان سال:

        </td>
        <td width="20%">
                          <cc1:CustomRadComboBox ID="cmbYear" runat="server" AppendDataBoundItems="True" 
                              Width="300px" CausesValidation="False"></cc1:CustomRadComboBox>
                              <asp:RequiredFieldValidator ID="rfvYear" runat="server" ControlToValidate="cmbYear"
                                ValidationGroup="StructureDate" ErrorMessage="*"></asp:RequiredFieldValidator>
        </td>
                              <td nowrap="nowrap" width="10%">
                            ساختار سازمانی:
                                               
                        </td>
                        <td width="20%">
                            <cc1:SelectControl runat="server" PortalPathUrl="GeneralProject/General/ModalForm/OrganizationPhysicalChartPage.aspx"
                                imageName="OrganizationPhysicalChart" 
                                ID="SelectControlOrganizationPhysicalChart" SelectControlClear="True"></cc1:SelectControl>
                            </td>
                            <td width="40%">
                                     <asp:ImageButton ID="imgbtnAddDate" runat="server" ImageUrl="../Images/AddDate.png"
                                ValidationGroup="StructureDate" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"
                                Height="30px" OnClick="imgbtnAddDate_Click" Width="30px" />   
                            </td>
    </tr>
    <tr>
        <td colspan="5">
<div align="center" style="width:100%">
<asp:PlaceHolder ID="PlaceHolderStructureCustomCalendar" runat="server"></asp:PlaceHolder>
</div>            
        </td>
    </tr>
</table>

</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="imgbtnAddDate">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGeneral" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
    Skin="Default">
</telerik:RadAjaxLoadingPanel>

