﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StatuesHolidayCustomCalendarPage.ascx.cs" Inherits="Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation.StatuesHolidayCustomCalendarPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<table style="width: 100%">
                <tr>
                    
                         <td align="center" valign="middle" bgcolor="#006699">
                        <asp:Label ID="LabFormName" runat="server" Text="وضیعت تعطیلی" 
                            Font-Bold="True" Font-Names="Arial"
                            Font-Size="16pt" ForeColor="White"></asp:Label>
                    </td>
                </tr>
            </table>

            <table style="width: 100%" bgcolor="AliceBlue">
            <tr>
            <td align="right" dir="rtl">
                <asp:Panel ID="pnlButton" runat="server">                
                          <table>
                                    <tr>
                  
                                        <td>
                                            <cc1:CustomRadButton ID="BtnEdit" runat="server" CustomeButtonType="Edit" 
                                                onclick="BtnEdit_Click">
                                            </cc1:CustomRadButton>

                                     
                                        </td>
             
                    
                                        <td nowrap="nowrap">
                                                   <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" /> 
                                        </td>
                                    </tr>
                                </table>

                                </asp:Panel>

            </td>
            </tr>
            <tr>
            <td align="right" dir="rtl">
                <asp:Panel ID="pnlDetail" runat="server">
                
            <table width="100%">
                                <tr>
                        <td width="15%">
                                <asp:Label runat="server" ID="lblIsHoliday">آیا تعطیل می باشد؟:</asp:Label>
                        </td>
                        <td>
                            <cc1:CustomRadComboBox ID="cmbIsHoliday" runat="server" AppendDataBoundItems="True">
                            <Items>
                            <telerik:RadComboBoxItem  runat="server" Text="" Value=""/>
                            <telerik:RadComboBoxItem runat="server" Text="خیر" Value="False" /><telerik:RadComboBoxItem runat="server" Text="بله" Value="True" /></Items></cc1:CustomRadComboBox>
                            <asp:RequiredFieldValidator ID="rfvIsHoliday" runat="server" ErrorMessage="*" 
                                ControlToValidate="cmbIsHoliday"></asp:RequiredFieldValidator>
                        </td>
                        </tr>
            </table>
            </asp:Panel>
            </td>
            </tr>
            </table>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="BtnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" Runat="server" 
    Skin="Default">
</telerik:RadAjaxLoadingPanel>
