﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using ITC.Library.Controls;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation
{
    public partial class StructureCustomCalendarPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MonthBL _monthBL = new MonthBL();
        private readonly YearBL _yearBL = new YearBL();
        private  readonly  SeasonBL _seasonBL=new SeasonBL();
        private readonly CustomCalendarBL _customCalendarBL = new CustomCalendarBL();
        readonly string[] _arrayWeekDayTitle = new string[7];
        #endregion

        #region procedure:

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add(new RadComboBoxItem(""));
            var yearEntity = new YearEntity()
            {
                CalendarTypeId = 1
            };
            cmbYear.DataSource = _yearBL.GetYearCollectionByCalendarType(yearEntity);
            cmbYear.DataTextField = "YearTitle";
            cmbYear.DataValueField = "YearId";
            cmbYear.DataBind();
        }

        private void SetStructureCustomCalendar()
        {
            const int tblRowGenteralCount = 2;
            const int tblColGeneralCount = 2;
            const int tblRowsSeasonCount = 2;
            const int tblColsSeasonCount = 1;
            ViewState["MonthNo"] = 1;
            byte SeasonNo = 1;
            var tblGeneral = new Table();
            tblGeneral.CssClass = "tblGeneral";
            var trGeneralTitle = new TableRow();
            var tcGeneralTitle = new TableCell();
            PlaceHolderStructureCustomCalendar.Controls.Add(tblGeneral);
            tcGeneralTitle.ColumnSpan = 2;
            tcGeneralTitle.CssClass = "tdGeneralTitle";
            tcGeneralTitle.Text = cmbYear.SelectedItem.Text;
            trGeneralTitle.Cells.Add(tcGeneralTitle);
            tblGeneral.Rows.Add(trGeneralTitle);

            for (var n = 0; n < tblRowGenteralCount; n++)
            {
                var trGeneral = new TableRow();
                for (var m = 0; m < tblColGeneralCount; m++)
                {
                    var tcGeneral = new TableCell();
                    tcGeneral.CssClass = "tdGeneral";
                    var tblSeason = new Table();
                    tblSeason.CssClass = "tblSeason";
                    trGeneral.Controls.Add(tcGeneral);
                    tcGeneral.Controls.Add(tblSeason);
                    for (int i = 0; i < tblRowsSeasonCount; i++)
                    {
                        TableRow trSeason = new TableRow();
                        for (int j = 0; j < tblColsSeasonCount; j++)
                        {
                            TableCell tcSeason = new TableCell();
                            tcSeason.CssClass = "tdSeason";
                            trSeason.Controls.Add(tcSeason);
                            if (i == 0)//عنوان فصل ها
                            {
                                SeasonEntity seasonEntity=new SeasonEntity()
                                {
                                    SeasonNo = SetSesonNo(n, m)
                                };

                                SeasonEntity mySeasonEntity=_seasonBL.GetSingleBySeasonNo(seasonEntity);
                                tcSeason.Text = mySeasonEntity.SeasonPersianTitle;
                                Color myColor = ColorTranslator.FromHtml(mySeasonEntity.BackGroundColor);
                                tcSeason.BackColor = myColor;
                                
                                trSeason.Cells.Add(tcSeason);
                            }
                            else
                            {
                                SeasonNo = SetSesonNo(n, m);
                                SetMonth(tcSeason, SeasonNo);
                            }
                        }
                        tblSeason.Rows.Add(trSeason);

                    }
                    tblGeneral.Rows.Add(trGeneral);
                }

            }
        }

        void SetMonth(TableCell tcSeason, byte seasonNo)
        {
            const int rowMonthCount = 2;
            const int colsMonthCount = 4;
            var tblMonth = new Table();
            tblMonth.CssClass = "tblMonth";
            tcSeason.Controls.Add(tblMonth);
            for (int i = 0; i < rowMonthCount; i++)
            {
                var trMonth = new TableRow();
                for (int j = 0; j < colsMonthCount; j++)
                {
                    var tcMonth = new TableCell();
                    trMonth.Controls.Add(tcMonth);
                    if (i == 0)
                    {
                        if (j == 0)
                        {
                            tcMonth.Text = "ایام هفته";
                            tcMonth.CssClass = "tdWeekday";
                        }
                        else
                        {
                         
                            SeasonEntity seasonEntity=new SeasonEntity()
                            {
                                SeasonNo = seasonNo
                            };
                            List<MonthEntity> list = _monthBL.GetMonthCollectionBySeasonNo(seasonEntity);
                            tcMonth.Text = list[j - 1].MonthTitle;            
                            tcMonth.CssClass = "tdMonth";
                            

                        }
                    }

                    else
                    {
                        if (j == 0)
                        {
                            tcMonth.CssClass = "tdWeekday";
                            WeekDay(tcMonth);
                        }
                        else
                        {
                            tcMonth.CssClass = "tdMonth";
                            SetDay(tcMonth);
                            ViewState["MonthNo"] = int.Parse(ViewState["MonthNo"].ToString()) + 1;
                        }
                    }
                }
               
                tblMonth.Controls.Add(trMonth);
            }
        }

        private void SetDay(TableCell tcMonth)
        {
            
            GenerationDateEntity generationDateEntity=new GenerationDateEntity()
            {
                YearId = int.Parse(cmbYear.SelectedValue),
                MonthNo = int.Parse(ViewState["MonthNo"].ToString()),
                OrganizationPhysicalChartId = Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId)
                
            };
           DataSet ds= _customCalendarBL.DayCalanderCustomGetAll(generationDateEntity);
            Table tblDay = new Table();
            tblDay.CssClass = "tblDay";
            tcMonth.Controls.Add(tblDay);
            var z = 0;
            var n = 0;
            for (int i = 0; i < 7; i++)
            {
                TableRow trDay = new TableRow();
                for (int j = 0; j < 6; j++)
                {
                    if (z > 41)
                    {
                        n = n + 1;
                        z = 0 + n;
                    }
                    TableCell tcDay = new TableCell();
                    tcDay.CssClass = "tdDay";
                    Button btn = new Button();                              
                    btn.CssClass = "btnDay";
                    btn.CausesValidation = false;
                    btn.Text = (ds.Tables[0].Rows[z][0].ToString() == "NULL" ? "" : SetNumberFarsi(ds.Tables[0].Rows[z][0].ToString()));
                    btn.Enabled = (ds.Tables[0].Rows[z][0].ToString() == "NULL" ? false : true);
                    if (ds.Tables[0].Rows[z][2].ToString() != "")
                        btn.ToolTip = ds.Tables[0].Rows[z][2].ToString();
                    btn.BackColor = (ds.Tables[0].Rows[z][3].ToString() == "NULL" ? System.Drawing.Color.White : ColorTranslator.FromHtml(ds.Tables[0].Rows[z][3].ToString()));

                    tcDay.Controls.Add(btn);
                    trDay.Cells.Add(tcDay);
                    z = z + 7;

                }

                tblDay.Rows.Add(trDay);
            }
        }

        byte SetSesonNo(int n, int m)
        {
            byte seasonNo = 1;
            if (n == 0 & m == 0)
                seasonNo = 1;
            if (n == 0 & m == 1)
                seasonNo = 2;
            if (n == 1 & m == 0)
                seasonNo = 3;
            if (n == 1 & m == 1)
                seasonNo = 4;
            return seasonNo;

        }

        void WeekDay(TableCell tcMonth)
        {
            _arrayWeekDayTitle[0] = "شنبه";
            _arrayWeekDayTitle[1] = "یکشنبه";
            _arrayWeekDayTitle[2] = "دوشنبه";
            _arrayWeekDayTitle[3] = "سه شنبه";
            _arrayWeekDayTitle[4] = "چهارشنبه";
            _arrayWeekDayTitle[5] = "پنج شنبه";
            _arrayWeekDayTitle[6] = "جمعه";
            const int rowWeekDayCount = 7;
            const int colsWeekDayCount = 1;

            var tblWeekDay = new Table {CssClass = "tblWeekday"};
            tcMonth.Controls.Add(tblWeekDay);
            for (int i = 0; i < rowWeekDayCount; i++)
            {
                var trWeekDay = new TableRow();
                for (int j = 0; j < colsWeekDayCount; j++)
                {
                    var tcWeekDay = new TableCell {CssClass = "tdWeekday", Text = _arrayWeekDayTitle[i]};

                    trWeekDay.Cells.Add(tcWeekDay);
                }
                tblWeekDay.Rows.Add(trWeekDay);
            }
        }
        private string SetNumberFarsi(string NumberEnglish)
        {
            string NumberFarsi = NumberEnglish;
            NumberFarsi = NumberFarsi.Replace("0", ((char)1776).ToString());
            NumberFarsi = NumberFarsi.Replace("1", ((char)1777).ToString());
            NumberFarsi = NumberFarsi.Replace("2", ((char)1778).ToString());
            NumberFarsi = NumberFarsi.Replace("3", ((char)1779).ToString());
            NumberFarsi = NumberFarsi.Replace("4", ((char)1780).ToString());
            NumberFarsi = NumberFarsi.Replace("5", ((char)1781).ToString());
            NumberFarsi = NumberFarsi.Replace("6", ((char)1782).ToString());
            NumberFarsi = NumberFarsi.Replace("7", ((char)1783).ToString());
            NumberFarsi = NumberFarsi.Replace("8", ((char)1784).ToString());
            NumberFarsi = NumberFarsi.Replace("9", ((char)1785).ToString());
            return NumberFarsi;

        }
        private void CheckValidation()
        {
            if (SelectControlOrganizationPhysicalChart.KeyId == "")
                throw (new ItcApplicationErrorManagerException("ساختار سازمانی را انتخاب نمایید"));
        }
        #endregion

                #region ControlEvent:
                /// <summary>
                /// پیاده سازی PageLoad
                /// </summary>

            public override
            void InitControl()
        {
            try
            {

                SetComboBox();
            }
            catch (Exception ex)
            {


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion


        protected void imgbtnAddDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CheckValidation();
                SetStructureCustomCalendar();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl1.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
            
        }


    }
}