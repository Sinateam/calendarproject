﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.CalendarProject.Calendar.CalendarInformation
{
    public partial class CustomCalendarPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MonthBL _monthBL = new MonthBL();
        private readonly YearBL _yearBL = new YearBL();
        private readonly CalendarTypeBL _calendarTypeBL = new CalendarTypeBL();
        private readonly GeneralCalendarBL _generalCalendarBL = new GeneralCalendarBL();
        private readonly WeekDayBL _weekDayBL = new WeekDayBL();
        private const string TableName = "[Calendar].[V_GeneralCalendarCustome]";
        private const string PrimaryKey = "CustomCalendarId";

        #endregion
        #region Procedure:



        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            
            cmbYear.ClearSelection();
            SelectControlOrganizationPhysicalChart.KeyId = "";
            SelectControlOrganizationPhysicalChart.Title = "";
            SelectControlOrganizationPhysicalChartSearch.KeyId = "";
            SelectControlOrganizationPhysicalChartSearch.Title = "";
            cmbIsHoliday.ClearSelection();
            cmbMonthTitle.ClearSelection();
            cmbWeekDayTitle.ClearSelection();
            cmbYearSearch.ClearSelection();
            ;



        }


        void SetpnlGenerateDate()
        {
            pnlGenerateDate.Visible = true;
            pnlCopyGeneratDateDetail.Visible = false;
            pnlCopyGeneratDate.Visible = false;
        }

        void SetPnlDetail()
        {
            pnlGenerateDate.Visible = true;
            pnlCopyGeneratDate.Visible = true;
            pnlCopyGeneratDateDetail.Visible = false;
        }




        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdGeneralCalendar,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }

        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbYear.Items.Clear();
            cmbYear.Items.Add(new RadComboBoxItem(""));
            YearEntity yearEntity=new YearEntity()
            {
                CalendarTypeId =1
            };
            cmbYear.DataSource = _yearBL.GetYearCollectionByCalendarType(yearEntity);
            cmbYear.DataTextField = "YearTitle";
            cmbYear.DataValueField = "YearId";
            cmbYear.DataBind();



        }
        /// <summary>
        /// لود شدن اطلاعات در لیست بازشونده زمان لود شدن پنل اطلاعات دیگر
        /// </summary>
        private void SetComboBoxOtherInfo()
        {
            cmbMonthTitle.Items.Clear();
            cmbMonthTitle.Items.Add(new RadComboBoxItem(""));
            cmbMonthTitle.DataSource = _monthBL.GetAllIsActive();
            cmbMonthTitle.DataTextField = "MonthTitle";
            cmbMonthTitle.DataValueField = "MonthId";
            cmbMonthTitle.DataBind();

            cmbWeekDayTitle.Items.Clear();
            cmbWeekDayTitle.Items.Add(new RadComboBoxItem(""));
            cmbWeekDayTitle.DataSource = _weekDayBL.GetAllIsActive();
            cmbWeekDayTitle.DataTextField = "WeekDayPersianTitle";
            cmbWeekDayTitle.DataValueField = "WeekDayId";
            cmbWeekDayTitle.DataBind();

            cmbYearSearch.Items.Clear();
            cmbYearSearch.Items.Add(new RadComboBoxItem(""));
            YearEntity yearEntity = new YearEntity()
            {
                CalendarTypeId = 1
            };
            cmbYearSearch.DataSource = _yearBL.GetYearCollectionByCalendarType(yearEntity);
            cmbYearSearch.DataTextField = "YearTitle";
            cmbYearSearch.DataValueField = "YearId";
            cmbYearSearch.DataBind();

        }




        private void CheckValidation()
        {
            if (SelectControlOrganizationPhysicalChart.KeyId == "")
                throw new ItcApplicationErrorManagerException("ساختار سازمانی را انتخاب نمایید");
        }

        private void AddPageView(RadTab tab)
        {
            var pageView = new RadPageView();
            pageView.ID = tab.PageViewID;
            radmultipageCalander.PageViews.Add(pageView);

        }
        #endregion

        #region ControlEvent:
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {

                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                SetClearToolBox();
                SetComboBox();
                SetpnlGenerateDate();
                grdGeneralCalendar.MasterTableView.GetColumnSafe("_ShowAll").Visible = false;
            }
            catch (Exception ex)
            {


            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion





        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (cmbMonthTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ((MonthPersianId = '" + (cmbMonthTitle.SelectedValue) + "') OR ( MonthArabicId = '" + (cmbMonthTitle.SelectedValue) + "') OR (MonthEnglishId= '" + cmbMonthTitle.SelectedValue + "'))";
                if (cmbWeekDayTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WeekDayId= '" + cmbWeekDayTitle.SelectedValue + "'";
                if (cmbIsHoliday.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and IsHoliday = '" + (cmbIsHoliday.SelectedItem.Value) + "'";
                if (txtDayNo.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ((PersianDayNo = '" + (txtDayNo.Text) + "') OR ( ArabicDayNo = '" + (txtDayNo.Text) + "') OR (EnglishDayNo= '" + txtDayNo.Text + "'))";
                if (txtMiladiDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ShamsiDate= '" + txtMiladiDate.Text + "'";
                if (cmbYearSearch.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ((YearPersianId = '" + (cmbYearSearch.SelectedValue) + "') OR ( YearArabicId = '" + (cmbYearSearch.SelectedValue) + "') OR (YearEnglishId= '" + cmbYearSearch.SelectedValue + "'))";

                if (SelectControlOrganizationPhysicalChartSearch.KeyId != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OrganizationPhysicalChartId= '" +
                                               Guid.Parse(SelectControlOrganizationPhysicalChartSearch.KeyId) + "'";
                grdGeneralCalendar.MasterTableView.CurrentPageIndex = 0;
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdGeneralCalendar.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl1.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl1.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }



        }


        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {                
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        

        protected void ImageCopyDate_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                CheckValidation();
                ViewState["WhereClause"] = "";
                var generationDateEntity = new GenerationDateEntity()
                {
                    YearId = int.Parse(cmbYear.SelectedValue),                    
                    OrganizationPhysicalChartId = Guid.Parse(SelectControlOrganizationPhysicalChart.KeyId)
                };
                _generalCalendarBL.GenerationDateCopy(generationDateEntity);
                SetClearToolBox();
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void imgbtnOtherInfo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                SetComboBoxOtherInfo();
                SetPnlDetail();
                ViewState["WhereClause"] = "1=1";
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdGeneralCalendar_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "OtherInformation")
            {
                pnlGenerateDate.Visible = false;
                radmultipageCalander.PageViews.Clear();
                pnlCopyGeneratDateDetail.Visible = true;
                var CustomCalendarId = int.Parse(e.CommandArgument.ToString());
                Session["CustomCalendarId"] = CustomCalendarId;

                ViewState["WhereClause"] = "CustomCalendarId='" + CustomCalendarId + "'";
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, CustomCalendarId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                e.Item.Selected = true;
                grdGeneralCalendar.MasterTableView.GetColumnSafe("_ShowAll").Visible = true;
                radtabCalender_TabClick(new object(), new RadTabStripEventArgs(radtabCalender.FindTabByValue("StatuesHolidayCustomCalendarPage")));
            }
            if (e.CommandName == "_ShowAll")
            {
                try
                {
                    pnlGenerateDate.Visible = true;
                    pnlCopyGeneratDateDetail.Visible = false;
                    ViewState["WhereClause"] = "";
                    var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    grdGeneralCalendar.MasterTableView.GetColumnSafe("_ShowAll").Visible = false;
                }
                catch (Exception ex)
                {
                    CustomMessageErrorControl.ShowErrorMessage(ex.Message);
                }
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdGeneralCalendar_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdGeneralCalendar_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdGeneralCalendar_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdGeneralCalendar.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        protected void radtabCalender_TabClick(object sender, RadTabStripEventArgs e)
        {
            try
            {
                e.Tab.Selected = true;
                radmultipageCalander.PageViews.Clear();
                if (e.Tab.PageView == null)
                {
                    AddPageView(e.Tab);
                }
                ((radmultipageCalander.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl).InitControl();
            }

            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void radmultipageCalander_PageViewCreated(object sender, RadMultiPageEventArgs e)
        {
            string userControlName = @"DeskTopModules\CalendarProject\Calendar\" + e.PageView.ID + ".ascx";

            var userControl = (Page.LoadControl(userControlName));

            if (userControl != null)
            {
                userControl.ID = e.PageView.ID.Replace(@"\", "") + "_userControl";
            }

            e.PageView.Controls.Add(userControl);
            e.PageView.Selected = true;
        }

        

    }
}