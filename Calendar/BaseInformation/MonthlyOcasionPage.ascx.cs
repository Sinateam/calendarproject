﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت در ماه>
    /// </summary>
    public partial class MonthlyOcasionPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly MonthBL _monthBL = new MonthBL();
        private readonly OccasionDayBL _occasionDayBL = new OccasionDayBL();
        private readonly MonthlyOcasionBL _monthlyOcasionBL = new MonthlyOcasionBL();
        private const string TableName = "Calendar.V_MonthlyOcasion";
        private const string PrimaryKey = "MonthlyOcasionId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtEndDayNo.Text = "";
            txtSatrtDayNo.Text = "";
            cmbMonth.ClearSelection();
            cmbOccasionDay.ClearSelection();
            cmbIsFirstDayOfMonth.SelectedIndex = 2;
            cmbIsLastDayOfMonth.SelectedIndex = 2;


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var monthlyOcasionEntity = new MonthlyOcasionEntity()
            {
                MonthlyOcasionId = int.Parse(ViewState["MonthlyOcasionId"].ToString())
            };
            var mymonthlyOcasion = _monthlyOcasionBL.GetSingleById(monthlyOcasionEntity);
            txtEndDayNo.Text = mymonthlyOcasion.EndDayNo.ToString();
            txtSatrtDayNo.Text = mymonthlyOcasion.SatrtDayNo.ToString();
            cmbIsFirstDayOfMonth.SelectedValue = mymonthlyOcasion.IsFirstDayOfMonth.ToString();
            cmbIsLastDayOfMonth.SelectedValue = mymonthlyOcasion.IsLastDayOfMonth.ToString();                        
            if (cmbOccasionDay.FindItemByValue(mymonthlyOcasion.OccasionDayId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردمناسبت انتخاب شده معتبر نمی باشد.");
            }

            cmbOccasionDay.SelectedValue = mymonthlyOcasion.OccasionDayId.ToString();
            if (cmbMonth.FindItemByValue(mymonthlyOcasion.MonthId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکورد ماه انتخاب شده معتبر نمی باشد.");
            }
            cmbMonth.SelectedValue = mymonthlyOcasion.MonthId.ToString();            
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdMonthlyOcasion,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }


        /// <summary>
        /// لود کردن اطلاعات  در لیست باز شونده
        /// </summary>
        private void SetComboBox()
        {

            cmbOccasionDay.Items.Clear();
            cmbOccasionDay.Items.Add(new RadComboBoxItem(""));
            cmbOccasionDay.DataSource = _occasionDayBL.GetAll();
            cmbOccasionDay.DataTextField = "OccasionDayTitle";
            cmbOccasionDay.DataValueField = "OccasionDayId";
            cmbOccasionDay.DataBind();

            cmbMonth.Items.Clear();
            cmbMonth.Items.Add(new RadComboBoxItem(""));
            cmbMonth.DataSource = _monthBL.GetAllIsActive();
            cmbMonth.DataTextField = "MonthTitle";
            cmbMonth.DataValueField = "MonthId";
            cmbMonth.DataBind();

        }

        #endregion

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int monthlyOcasionId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();
                var monthlyOcasionEntity = new MonthlyOcasionEntity()
                {
                    OccasionDayId = int.Parse(cmbOccasionDay.SelectedValue),
                    MonthId = int.Parse(cmbMonth.SelectedValue),
                    SatrtDayNo = (txtSatrtDayNo.Text==""?(int?)null:int.Parse(txtSatrtDayNo.Text)),
                    EndDayNo = (txtEndDayNo.Text==""?(int?)null:int.Parse(txtEndDayNo.Text)),
                    IsFirstDayOfMonth = (cmbIsFirstDayOfMonth.SelectedIndex==0?(bool?)null:bool.Parse(cmbIsFirstDayOfMonth.SelectedItem.Value)),
                    IsLastDayOfMonth = (cmbIsLastDayOfMonth.SelectedIndex==0?(bool?)null:bool.Parse(cmbIsLastDayOfMonth.SelectedItem.Value)),                    
                };

                _monthlyOcasionBL.Add(monthlyOcasionEntity, out monthlyOcasionId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, monthlyOcasionId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtEndDayNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EndDayNo= '" +
                                               txtEndDayNo.Text + "'";

                if (txtSatrtDayNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SatrtDayNo= '" +
                                               txtSatrtDayNo.Text + "'";

                if (cmbMonth.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and MonthId= '" +
                                               int.Parse(cmbMonth.SelectedValue) + "'";
                if (cmbOccasionDay.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayId= '" +
                                               int.Parse(cmbOccasionDay.SelectedValue) + "'";
                
                if (cmbIsFirstDayOfMonth.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsFirstDayOfMonth='" +
                                               (cmbIsFirstDayOfMonth.SelectedItem.Value.Trim()) + "'";

                if (cmbIsLastDayOfMonth.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsLastDayOfMonth='" +
                                               (cmbIsLastDayOfMonth.SelectedItem.Value.Trim()) + "'";
                grdMonthlyOcasion.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdMonthlyOcasion.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var monthlyOcasionEntity = new MonthlyOcasionEntity()
                {
                    OccasionDayId = int.Parse(cmbOccasionDay.SelectedValue),
                    MonthId = int.Parse(cmbMonth.SelectedValue),
                    SatrtDayNo = (txtSatrtDayNo.Text == "" ? (int?)null : int.Parse(txtSatrtDayNo.Text)),
                    EndDayNo = (txtEndDayNo.Text == "" ? (int?)null : int.Parse(txtEndDayNo.Text)),
                    IsFirstDayOfMonth = (cmbIsFirstDayOfMonth.SelectedIndex == 0 ? (bool?)null : bool.Parse(cmbIsFirstDayOfMonth.SelectedItem.Value)),
                    IsLastDayOfMonth = (cmbIsLastDayOfMonth.SelectedIndex == 0 ? (bool?)null : bool.Parse(cmbIsLastDayOfMonth.SelectedItem.Value)),
                    MonthlyOcasionId = int.Parse(ViewState["MonthlyOcasionId"].ToString())

                };
                _monthlyOcasionBL.Update(monthlyOcasionEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, int.Parse(ViewState["MonthlyOcasionId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdMonthlyOcasion_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["MonthlyOcasionId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var monthlyOcasionEntity = new MonthlyOcasionEntity()
                    {
                        MonthlyOcasionId = int.Parse(e.CommandArgument.ToString()),
                    };
                    _monthlyOcasionBL.Delete(monthlyOcasionEntity);
                    var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        

        protected void grdMonthlyOcasion_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdMonthlyOcasion_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdMonthlyOcasion_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdMonthlyOcasion.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}