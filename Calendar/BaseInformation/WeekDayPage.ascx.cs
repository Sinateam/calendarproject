﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <اطلاعات هفته>
    /// </summary>
    public partial class WeekDayPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly WeekDayBL _weekDayBL = new WeekDayBL();
        private const string TableName = "Calendar.t_WeekDay";
        private const string PrimaryKey = "WeekDayId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtWeekDayArabicTitle.Text = "";
            txtWeekDayEnglishTitle.Text = "";
            txtWeekDayPersianTitle.Text = "";
            txtWeekDayNo.Text = "";
            txtBackGroundColor.Text = "";
            cmbIsWeekEnd.SelectedIndex = 2;
            cmbIsActive.SelectedIndex = 1;


        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var weekDayEntity = new WeekDayEntity()
            {
                WeekDayId = int.Parse(ViewState["WeekDayId"].ToString())
            };
            var myWeelDay = _weekDayBL.GetSingleById(weekDayEntity);
            txtWeekDayPersianTitle.Text = myWeelDay.WeekDayPersianTitle;
            cmbIsActive.SelectedValue = myWeelDay.IsActive.ToString();
            txtWeekDayArabicTitle.Text = myWeelDay.WeekDayArabicTitle;
            txtWeekDayEnglishTitle.Text = myWeelDay.WeekDayEnglishTitle;
            cmbIsWeekEnd.SelectedValue = myWeelDay.IsWeekEnd.ToString();
            txtBackGroundColor.Text = myWeelDay.BackGroundColor.ToString();
            txtWeekDayNo.Text = myWeelDay.WeekDayNo.ToString();


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdWeekDay,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }



        #endregion

        #region ControlEvent:
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>

        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int weekDayId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();
                var weekDayEntity = new WeekDayEntity()
                {
                    WeekDayPersianTitle = txtWeekDayPersianTitle.Text,
                    WeekDayEnglishTitle = txtWeekDayEnglishTitle.Text,
                    WeekDayArabicTitle = txtWeekDayArabicTitle.Text,
                    BackGroundColor = txtBackGroundColor.Text,
                    WeekDayNo = byte.Parse(txtWeekDayNo.Text),
                    IsWeekEnd = bool.Parse(cmbIsWeekEnd.SelectedItem.Value),                    
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };

                _weekDayBL.Add(weekDayEntity, out weekDayId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, weekDayId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtWeekDayPersianTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WeekDayPersianTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtWeekDayPersianTitle.Text.Trim()) + "%'";
                if (txtWeekDayEnglishTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WeekDayEnglishTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtWeekDayEnglishTitle.Text.Trim()) + "%'";
                if (txtWeekDayArabicTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and WeekDayArabicTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtWeekDayArabicTitle.Text.Trim()) + "%'";
                if (txtBackGroundColor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BackGroundColor Like N'%" +
                                               FarsiToArabic.ToArabic(txtBackGroundColor.Text.Trim()) + "%'";
                if (txtWeekDayNo.Text!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And WeekDayNo='" +
                                               (txtWeekDayNo.Text.Trim()) + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                if (cmbIsWeekEnd.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsWeekEnd='" +
                                               (cmbIsWeekEnd.SelectedItem.Value.Trim()) + "'";
                grdWeekDay.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdWeekDay.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var weekDayEntity = new WeekDayEntity()
                {
                    WeekDayId = int.Parse(ViewState["WeekDayId"].ToString()),
                    WeekDayPersianTitle = txtWeekDayPersianTitle.Text,
                    WeekDayEnglishTitle = txtWeekDayEnglishTitle.Text,
                    WeekDayArabicTitle = txtWeekDayArabicTitle.Text,
                    BackGroundColor = txtBackGroundColor.Text,
                    WeekDayNo = byte.Parse(txtWeekDayNo.Text),
                    IsWeekEnd = bool.Parse(cmbIsWeekEnd.SelectedItem.Value),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _weekDayBL.Update(weekDayEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, int.Parse(ViewState["WeekDayId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdWeekDay_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["WeekDayId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var weekDayEntity = new WeekDayEntity()
                    {
                        WeekDayId = int.Parse(e.CommandArgument.ToString()),
                    };
                    _weekDayBL.Delete(weekDayEntity);
                    var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

        protected void grdWeekDay_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdWeekDay_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdWeekDay_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdWeekDay.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

    }
}