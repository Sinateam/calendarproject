﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.CalendarProject.Calendar.BL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <نوع مناسبت>
    /// </summary>
    public partial class OccasionDayTypePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly OccasionDayTypeBL _occasionDayTypeBL = new OccasionDayTypeBL();
        private const string TableName = "Calendar.t_OccasionDayType";
        private const string PrimaryKey = "OccasionDayTypeId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtOccasionDayTypeTitle.Text = "";
            txtDescription.Text = "";
            txtPriority.Text = "";
            txtBackGroundColor.Text = "";
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var occasionDayTypeEntity = new OccasionDayTypeEntity()
            {
                OccasionDayTypeId = int.Parse(ViewState["OccasionDayTypeId"].ToString())
            };
            var myoccasionDayType = _occasionDayTypeBL.GetSingleById(occasionDayTypeEntity);
            txtBackGroundColor.Text = myoccasionDayType.BackGroundColor;
            cmbIsActive.SelectedValue = myoccasionDayType.IsActive.ToString();
            txtDescription.Text = myoccasionDayType.Description;
            txtOccasionDayTypeTitle.Text = myoccasionDayType.OccasionDayTypeTitle;
            txtPriority.Text = myoccasionDayType.Priority.ToString();


        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, int rowSelectId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdOccasionDayType,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectId = rowSelectId
            };
            return gridParamEntity;
        }



        #endregion

        #region ControlEvent:
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

        }
        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int occasionDayTypeId;
                ViewState["WhereClause"] = "";
                SetPanelFirst();
                var occasionDayTypeEntity = new OccasionDayTypeEntity()
                {
                    OccasionDayTypeTitle = txtOccasionDayTypeTitle.Text,
                    Description = txtDescription.Text,
                    BackGroundColor = txtBackGroundColor.Text,
                    Priority = byte.Parse(txtPriority.Text),                    
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };

                _occasionDayTypeBL.Add(occasionDayTypeEntity, out occasionDayTypeId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, occasionDayTypeId);
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (txtOccasionDayTypeTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and OccasionDayTypeTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtOccasionDayTypeTitle.Text.Trim()) + "%'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtBackGroundColor.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and BackGroundColor Like N'%" +
                                               FarsiToArabic.ToArabic(txtBackGroundColor.Text.Trim()) + "%'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                if (txtPriority.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Priority ='" +
                                               byte.Parse(txtPriority.Text.Trim()) + "'";
                grdOccasionDayType.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
                if (grdOccasionDayType.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var occasionDayTypeEntity = new OccasionDayTypeEntity()
                {
                    OccasionDayTypeId = int.Parse(ViewState["OccasionDayTypeId"].ToString()),
                    OccasionDayTypeTitle = txtOccasionDayTypeTitle.Text,
                    Description = txtDescription.Text,
                    BackGroundColor = txtBackGroundColor.Text,
                    Priority = byte.Parse(txtPriority.Text),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _occasionDayTypeBL.Update(occasionDayTypeEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, int.Parse(ViewState["OccasionDayTypeId"].ToString()));
                DatabaseManager.ItcGetPage(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
                /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdOccasionDayType_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
             try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["OccasionDayTypeId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var occasionDayTypeEntity = new OccasionDayTypeEntity()
                    {
                        OccasionDayTypeId = int.Parse(e.CommandArgument.ToString()),
                    };
                    _occasionDayTypeBL.Delete(occasionDayTypeEntity);
                    var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                    DatabaseManager.ItcGetPage(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        

        protected void grdOccasionDayType_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, e.NewPageIndex, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdOccasionDayType_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdOccasionDayType_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {

            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdOccasionDayType.MasterTableView.PageSize, 0, -1);
                DatabaseManager.ItcGetPage(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion


    }
}