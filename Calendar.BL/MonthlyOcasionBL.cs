﻿using System.Collections.Generic;
using Intranet.Common.ItcException;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت در ماه>
    /// </summary>

    public class MonthlyOcasionBL : DeskTopObj
    {
        private readonly MonthlyOcasionDB _MonthlyOcasionDB;

        public MonthlyOcasionBL()
        {
            _MonthlyOcasionDB = new MonthlyOcasionDB();
        }

        public void Add(MonthlyOcasionEntity MonthlyOcasionEntityParam, out int MonthlyOcasionId)
        {
            if(MonthlyOcasionEntityParam.SatrtDayNo==null && MonthlyOcasionEntityParam.EndDayNo==null && MonthlyOcasionEntityParam.IsFirstDayOfMonth==false  && MonthlyOcasionEntityParam.IsLastDayOfMonth==false)
                throw (new ItcApplicationErrorManagerException("اطلاعات ناقص پر شده است"));
            if (MonthlyOcasionEntityParam.IsFirstDayOfMonth == true &&
                MonthlyOcasionEntityParam.IsLastDayOfMonth == true)
                throw (new ItcApplicationErrorManagerException("اطلاعات مناسبت ماه نمی تواند اول ماه و هم آخر ماه باشد"));
            _MonthlyOcasionDB.AddMonthlyOcasionDB(MonthlyOcasionEntityParam, out MonthlyOcasionId);
        }

        public void Update(MonthlyOcasionEntity MonthlyOcasionEntityParam)
        {
            if (MonthlyOcasionEntityParam.SatrtDayNo == null && MonthlyOcasionEntityParam.EndDayNo == null && MonthlyOcasionEntityParam.IsFirstDayOfMonth == false && MonthlyOcasionEntityParam.IsLastDayOfMonth == false)
                throw (new ItcApplicationErrorManagerException("اطلاعات ناقص پر شده است"));
            if (MonthlyOcasionEntityParam.IsFirstDayOfMonth == true &&
    MonthlyOcasionEntityParam.IsLastDayOfMonth == true)
                throw (new ItcApplicationErrorManagerException("اطلاعات مناسبت ماه نمی تواند اول ماه و هم آخر ماه باشد"));
            _MonthlyOcasionDB.UpdateMonthlyOcasionDB(MonthlyOcasionEntityParam);
        }

        public void Delete(MonthlyOcasionEntity MonthlyOcasionEntityParam)
        {
            _MonthlyOcasionDB.DeleteMonthlyOcasionDB(MonthlyOcasionEntityParam);
        }

        public MonthlyOcasionEntity GetSingleById(MonthlyOcasionEntity MonthlyOcasionEntityParam)
        {
            MonthlyOcasionEntity o = GetMonthlyOcasionFromMonthlyOcasionDB(
                _MonthlyOcasionDB.GetSingleMonthlyOcasionDB(MonthlyOcasionEntityParam));

            return o;
        }

        public List<MonthlyOcasionEntity> GetAll()
        {
            List<MonthlyOcasionEntity> lst = new List<MonthlyOcasionEntity>();
            //string key = "MonthlyOcasion_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MonthlyOcasionEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMonthlyOcasionCollectionFromMonthlyOcasionDBList(
                _MonthlyOcasionDB.GetAllMonthlyOcasionDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<MonthlyOcasionEntity> GetAllPaging(int currentPage, int pageSize,
            string

          sortExpression, out int count, string whereClause)
        {
            //string key = "MonthlyOcasion_List_Page_" + currentPage ;
            //string countKey = "MonthlyOcasion_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MonthlyOcasionEntity> lst = new List<MonthlyOcasionEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MonthlyOcasionEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMonthlyOcasionCollectionFromMonthlyOcasionDBList(
                _MonthlyOcasionDB.GetPageMonthlyOcasionDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private MonthlyOcasionEntity GetMonthlyOcasionFromMonthlyOcasionDB(MonthlyOcasionEntity o)
        {
            if (o == null)
                return null;
            MonthlyOcasionEntity ret = new MonthlyOcasionEntity(o.MonthlyOcasionId, o.MonthId, o.OccasionDayId,
                o.SatrtDayNo, o.EndDayNo, o.IsFirstDayOfMonth, o.IsLastDayOfMonth,o.CreationDate,o.ModificationDate);
            return ret;
        }

        private List<MonthlyOcasionEntity> GetMonthlyOcasionCollectionFromMonthlyOcasionDBList(
            List<MonthlyOcasionEntity> lst)
        {
            List<MonthlyOcasionEntity> RetLst = new List<MonthlyOcasionEntity>();
            foreach (MonthlyOcasionEntity o in lst)
            {
                RetLst.Add(GetMonthlyOcasionFromMonthlyOcasionDB(o));
            }
            return RetLst;

        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionCollectionByMonth(
            MonthlyOcasionEntity monthlyOcasionEntityParam)
        {
            return
                GetMonthlyOcasionCollectionFromMonthlyOcasionDBList(
                    _MonthlyOcasionDB.GetMonthlyOcasionDBCollectionByMonthDB(monthlyOcasionEntityParam));
        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionCollectionByOccasionDay(
            MonthlyOcasionEntity monthlyOcasionEntityParam)
        {
            return
                GetMonthlyOcasionCollectionFromMonthlyOcasionDBList(
                    _MonthlyOcasionDB.GetMonthlyOcasionDBCollectionByOccasionDayDB(monthlyOcasionEntityParam));
        }
    }

}
