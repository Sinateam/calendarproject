﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <نوع مناسبت>
    /// </summary>


	public class OccasionDayTypeBL : DeskTopObj
	{	
	  	 private readonly OccasionDayTypeDB _OccasionDayTypeDB;					
			
		public OccasionDayTypeBL()
		{
			_OccasionDayTypeDB = new OccasionDayTypeDB();
		}			
	
		public  void Add(OccasionDayTypeEntity  OccasionDayTypeEntityParam,out int OccasionDayTypeId)
		{ 
			_OccasionDayTypeDB.AddOccasionDayTypeDB(OccasionDayTypeEntityParam,out OccasionDayTypeId);			
		}

		public  void Update(OccasionDayTypeEntity  OccasionDayTypeEntityParam)
		{
			_OccasionDayTypeDB.UpdateOccasionDayTypeDB(OccasionDayTypeEntityParam);		
		}

		public  void Delete(OccasionDayTypeEntity  OccasionDayTypeEntityParam)
		{
			 _OccasionDayTypeDB.DeleteOccasionDayTypeDB(OccasionDayTypeEntityParam);			
		}

		public  OccasionDayTypeEntity GetSingleById(OccasionDayTypeEntity  OccasionDayTypeEntityParam)
		{
			OccasionDayTypeEntity o = GetOccasionDayTypeFromOccasionDayTypeDB(
			_OccasionDayTypeDB.GetSingleOccasionDayTypeDB(OccasionDayTypeEntityParam));
			
			return o;
		}

		public  List<OccasionDayTypeEntity> GetAll()
		{
			List<OccasionDayTypeEntity> lst = new List<OccasionDayTypeEntity>();
			//string key = "OccasionDayType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OccasionDayTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetOccasionDayTypeCollectionFromOccasionDayTypeDBList(
				_OccasionDayTypeDB.GetAllOccasionDayTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}


        public List<OccasionDayTypeEntity> GetAllIsActive()
        {
            List<OccasionDayTypeEntity> lst = new List<OccasionDayTypeEntity>();
            //string key = "OccasionDayType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionDayTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasionDayTypeCollectionFromOccasionDayTypeDBList(
            _OccasionDayTypeDB.GetAllOccasionDayTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

		public  List<OccasionDayTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "OccasionDayType_List_Page_" + currentPage ;
			//string countKey = "OccasionDayType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<OccasionDayTypeEntity> lst = new List<OccasionDayTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<OccasionDayTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetOccasionDayTypeCollectionFromOccasionDayTypeDBList(
				_OccasionDayTypeDB.GetPageOccasionDayTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  OccasionDayTypeEntity GetOccasionDayTypeFromOccasionDayTypeDB(OccasionDayTypeEntity o)
		{
	if(o == null)
                return null;
			OccasionDayTypeEntity ret = new OccasionDayTypeEntity(o.OccasionDayTypeId ,o.OccasionDayTypeTitle ,o.Priority ,o.BackGroundColor ,o.Description ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<OccasionDayTypeEntity> GetOccasionDayTypeCollectionFromOccasionDayTypeDBList( List<OccasionDayTypeEntity> lst)
		{
			List<OccasionDayTypeEntity> RetLst = new List<OccasionDayTypeEntity>();
			foreach(OccasionDayTypeEntity o in lst)
			{
				RetLst.Add(GetOccasionDayTypeFromOccasionDayTypeDB(o));
			}
			return RetLst;
			
		}


        
	}





}
