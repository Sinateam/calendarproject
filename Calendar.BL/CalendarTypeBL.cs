﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/26>
    /// Description: <نوع تقویم>
    /// </summary>


	public class CalendarTypeBL : DeskTopObj
	{	
	  	 private readonly CalendarTypeDB _CalendarTypeDB;					
			
		public CalendarTypeBL()
		{
			_CalendarTypeDB = new CalendarTypeDB();
		}			
	
		public  void Add(CalendarTypeEntity  CalendarTypeEntityParam,out int CalendarTypeId)
		{ 
			_CalendarTypeDB.AddCalendarTypeDB(CalendarTypeEntityParam,out CalendarTypeId);			
		}

		public  void Update(CalendarTypeEntity  CalendarTypeEntityParam)
		{
			_CalendarTypeDB.UpdateCalendarTypeDB(CalendarTypeEntityParam);		
		}

		public  void Delete(CalendarTypeEntity  CalendarTypeEntityParam)
		{
			 _CalendarTypeDB.DeleteCalendarTypeDB(CalendarTypeEntityParam);			
		}

		public  CalendarTypeEntity GetSingleById(CalendarTypeEntity  CalendarTypeEntityParam)
		{
			CalendarTypeEntity o = GetCalendarTypeFromCalendarTypeDB(
			_CalendarTypeDB.GetSingleCalendarTypeDB(CalendarTypeEntityParam));
			
			return o;
		}

		public  List<CalendarTypeEntity> GetAll()
		{
			List<CalendarTypeEntity> lst = new List<CalendarTypeEntity>();
			//string key = "CalendarType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CalendarTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetCalendarTypeCollectionFromCalendarTypeDBList(
				_CalendarTypeDB.GetAllCalendarTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}


        public List<CalendarTypeEntity> GetAllIsActive()
        {
            List<CalendarTypeEntity> lst = new List<CalendarTypeEntity>();
            //string key = "CalendarType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<CalendarTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetCalendarTypeCollectionFromCalendarTypeDBList(
            _CalendarTypeDB.GetAllCalendarTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<CalendarTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "CalendarType_List_Page_" + currentPage ;
			//string countKey = "CalendarType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<CalendarTypeEntity> lst = new List<CalendarTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CalendarTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetCalendarTypeCollectionFromCalendarTypeDBList(
				_CalendarTypeDB.GetPageCalendarTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  CalendarTypeEntity GetCalendarTypeFromCalendarTypeDB(CalendarTypeEntity o)
		{
	if(o == null)
                return null;
			CalendarTypeEntity ret = new CalendarTypeEntity(o.CalendarTypeId ,o.CalendarTypePersianTitle ,o.CalendarTypeEnglishTitle ,o.CalendarTypeArabicTitle ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<CalendarTypeEntity> GetCalendarTypeCollectionFromCalendarTypeDBList( List<CalendarTypeEntity> lst)
		{
			List<CalendarTypeEntity> RetLst = new List<CalendarTypeEntity>();
			foreach(CalendarTypeEntity o in lst)
			{
				RetLst.Add(GetCalendarTypeFromCalendarTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}



}

