﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <فصل>
    /// </summary>
        
	public class SeasonBL : DeskTopObj
	{	
	  	 private readonly SeasonDB _SeasonDB;					
			
		public SeasonBL()
		{
			_SeasonDB = new SeasonDB();
		}			
	
		public  void Add(SeasonEntity  SeasonEntityParam,out int SeasonId)
		{ 
			_SeasonDB.AddSeasonDB(SeasonEntityParam,out SeasonId);			
		}

		public  void Update(SeasonEntity  SeasonEntityParam)
		{
			_SeasonDB.UpdateSeasonDB(SeasonEntityParam);		
		}

		public  void Delete(SeasonEntity  SeasonEntityParam)
		{
			 _SeasonDB.DeleteSeasonDB(SeasonEntityParam);			
		}

		public  SeasonEntity GetSingleById(SeasonEntity  SeasonEntityParam)
		{
			SeasonEntity o = GetSeasonFromSeasonDB(
			_SeasonDB.GetSingleSeasonDB(SeasonEntityParam));
			
			return o;
		}
        public SeasonEntity GetSingleBySeasonNo(SeasonEntity seasonEntityParam)
        {
            SeasonEntity o = GetSeasonFromSeasonDB(
    _SeasonDB.GetSingleBySeasonNoDB(seasonEntityParam));

            return o;
        }
		public  List<SeasonEntity> GetAll()
		{
			List<SeasonEntity> lst = new List<SeasonEntity>();
			//string key = "Season_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SeasonEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSeasonCollectionFromSeasonDBList(
				_SeasonDB.GetAllSeasonDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<SeasonEntity> GetAllIsActive()
        {
            List<SeasonEntity> lst = new List<SeasonEntity>();
            //string key = "Season_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SeasonEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSeasonCollectionFromSeasonDBList(
            _SeasonDB.GetAllSeasonIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<SeasonEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Season_List_Page_" + currentPage ;
			//string countKey = "Season_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SeasonEntity> lst = new List<SeasonEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SeasonEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSeasonCollectionFromSeasonDBList(
				_SeasonDB.GetPageSeasonDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SeasonEntity GetSeasonFromSeasonDB(SeasonEntity o)
		{
	if(o == null)
                return null;
			SeasonEntity ret = new SeasonEntity(o.SeasonId ,o.SeasonPersianTitle ,o.SeasonEnglishTitle ,o.SeasonNo ,o.BackGroundColor ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<SeasonEntity> GetSeasonCollectionFromSeasonDBList( List<SeasonEntity> lst)
		{
			List<SeasonEntity> RetLst = new List<SeasonEntity>();
			foreach(SeasonEntity o in lst)
			{
				RetLst.Add(GetSeasonFromSeasonDB(o));
			}
			return RetLst;
			
		}


   
	}
}

