﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/31>
    /// Description: <تقویم>
    /// </summary>


    public class GeneralCalendarBL : DeskTopObj
    {
        private readonly GeneralCalendarDB _GeneralCalendarDB;

        public GeneralCalendarBL()
        {
            _GeneralCalendarDB = new GeneralCalendarDB();
        }

        public void Add(GeneralCalendarEntity GeneralCalendarEntityParam, out int GeneralCalendarId)
        {
            _GeneralCalendarDB.AddGeneralCalendarDB(GeneralCalendarEntityParam, out GeneralCalendarId);
        }

        public void Update(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            _GeneralCalendarDB.UpdateGeneralCalendarDB(GeneralCalendarEntityParam);
        }

        public void Delete(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            _GeneralCalendarDB.DeleteGeneralCalendarDB(GeneralCalendarEntityParam);
        }

        public GeneralCalendarEntity GetSingleById(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            GeneralCalendarEntity o = GetGeneralCalendarFromGeneralCalendarDB(
                _GeneralCalendarDB.GetSingleGeneralCalendarDB(GeneralCalendarEntityParam));

            return o;
        }

        public List<GeneralCalendarEntity> GetAll()
        {
            List<GeneralCalendarEntity> lst = new List<GeneralCalendarEntity>();
            //string key = "GeneralCalendar_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GeneralCalendarEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetGeneralCalendarCollectionFromGeneralCalendarDBList(
                _GeneralCalendarDB.GetAllGeneralCalendarDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<GeneralCalendarEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "GeneralCalendar_List_Page_" + currentPage ;
            //string countKey = "GeneralCalendar_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<GeneralCalendarEntity> lst = new List<GeneralCalendarEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<GeneralCalendarEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetGeneralCalendarCollectionFromGeneralCalendarDBList(
                _GeneralCalendarDB.GetPageGeneralCalendarDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private GeneralCalendarEntity GetGeneralCalendarFromGeneralCalendarDB(GeneralCalendarEntity o)
        {
            if (o == null)
                return null;
            GeneralCalendarEntity ret = new GeneralCalendarEntity(o.GeneralCalendarId, o.MiladiDate, o.YearPersianId,
                o.YearArabicId, o.YearEnglishId, o.MonthPersianId, o.MonthArabicId, o.MonthEnglishId, o.WeekDayId,
                o.PersianDayNo, o.ArabicDayNo, o.EnglishDayNo, o.Description, o.IsHoliday, o.CreationDate,
                o.ModificationDate);
            return ret;
        }

        private List<GeneralCalendarEntity> GetGeneralCalendarCollectionFromGeneralCalendarDBList(
            List<GeneralCalendarEntity> lst)
        {
            List<GeneralCalendarEntity> RetLst = new List<GeneralCalendarEntity>();
            foreach (GeneralCalendarEntity o in lst)
            {
                RetLst.Add(GetGeneralCalendarFromGeneralCalendarDB(o));
            }
            return RetLst;

        }



        public List<GeneralCalendarEntity> GetGeneralCalendarCollectionByMonth(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            return
                GetGeneralCalendarCollectionFromGeneralCalendarDBList(
                    _GeneralCalendarDB.GetGeneralCalendarDBCollectionByMonthDB(GeneralCalendarEntityParam));
        }



        public List<GeneralCalendarEntity> GetGeneralCalendarCollectionByWeekDay(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            return
                GetGeneralCalendarCollectionFromGeneralCalendarDBList(
                    _GeneralCalendarDB.GetGeneralCalendarDBCollectionByWeekDayDB(GeneralCalendarEntityParam));
        }

        public List<GeneralCalendarEntity> GetGeneralCalendarCollectionByYear(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            return
                GetGeneralCalendarCollectionFromGeneralCalendarDBList(
                    _GeneralCalendarDB.GetGeneralCalendarDBCollectionByYearDB(GeneralCalendarEntityParam));
        }

        public void GenerationDate(GenerationDateEntity generationDateEntity)
        {
            _GeneralCalendarDB.GenerationDateDB(generationDateEntity);
        }

        public void GenerationDateDelete(GenerationDateEntity generationDateEntity)
        {
            _GeneralCalendarDB.GenerationDateDeleteDB(generationDateEntity);
        }

        public void GenerationDateCopy(GenerationDateEntity generationDateEntity)
        {
            _GeneralCalendarDB.GenerationDateCopyDB(generationDateEntity);
        }
    }

}