﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <مشخصات ماه های سال>
    /// </summary>


    public class YearMonthBL : DeskTopObj
    {
        private readonly YearMonthDB _YearMonthDB;

        public YearMonthBL()
        {
            _YearMonthDB = new YearMonthDB();
        }

        public void Add(YearMonthEntity YearMonthEntityParam, out int YearMonthId)
        {
            _YearMonthDB.AddYearMonthDB(YearMonthEntityParam, out YearMonthId);
        }

        public void Update(YearMonthEntity YearMonthEntityParam)
        {
            _YearMonthDB.UpdateYearMonthDB(YearMonthEntityParam);
        }

        public void Delete(YearMonthEntity YearMonthEntityParam)
        {
            _YearMonthDB.DeleteYearMonthDB(YearMonthEntityParam);
        }

        public YearMonthEntity GetSingleById(YearMonthEntity YearMonthEntityParam)
        {
            YearMonthEntity o = GetYearMonthFromYearMonthDB(
                _YearMonthDB.GetSingleYearMonthDB(YearMonthEntityParam));

            return o;
        }

        public List<YearMonthEntity> GetAll()
        {
            List<YearMonthEntity> lst = new List<YearMonthEntity>();
            //string key = "YearMonth_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<YearMonthEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetYearMonthCollectionFromYearMonthDBList(
                _YearMonthDB.GetAllYearMonthDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<YearMonthEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "YearMonth_List_Page_" + currentPage ;
            //string countKey = "YearMonth_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<YearMonthEntity> lst = new List<YearMonthEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<YearMonthEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetYearMonthCollectionFromYearMonthDBList(
                _YearMonthDB.GetPageYearMonthDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private YearMonthEntity GetYearMonthFromYearMonthDB(YearMonthEntity o)
        {
            if (o == null)
                return null;
            YearMonthEntity ret = new YearMonthEntity(o.YearMonthId, o.YearId, o.MonthId, o.DayNo, o.IsActive,
                o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<YearMonthEntity> GetYearMonthCollectionFromYearMonthDBList(List<YearMonthEntity> lst)
        {
            List<YearMonthEntity> RetLst = new List<YearMonthEntity>();
            foreach (YearMonthEntity o in lst)
            {
                RetLst.Add(GetYearMonthFromYearMonthDB(o));
            }
            return RetLst;

        }



        public List<YearMonthEntity> GetYearMonthCollectionByMonth(YearMonthEntity yearMonthEntityParam)
        {
            return
                GetYearMonthCollectionFromYearMonthDBList(
                    _YearMonthDB.GetYearMonthDBCollectionByMonthDB(yearMonthEntityParam));
        }

        public List<YearMonthEntity> GetYearMonthCollectionByYear(YearMonthEntity yearMonthEntityParam)
        {
            return
                GetYearMonthCollectionFromYearMonthDBList(
                    _YearMonthDB.GetYearMonthDBCollectionByYearDB(yearMonthEntityParam));
        }


    }

}