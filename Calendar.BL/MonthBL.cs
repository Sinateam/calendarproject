﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <ماه>
    /// </summary>

    public class MonthBL : DeskTopObj
    {
        private readonly MonthDB _MonthDB;

        public MonthBL()
        {
            _MonthDB = new MonthDB();
        }

        public void Add(MonthEntity MonthEntityParam, out int MonthId)
        {
            _MonthDB.AddMonthDB(MonthEntityParam, out MonthId);
        }

        public void Update(MonthEntity MonthEntityParam)
        {
            _MonthDB.UpdateMonthDB(MonthEntityParam);
        }

        public void Delete(MonthEntity MonthEntityParam)
        {
            _MonthDB.DeleteMonthDB(MonthEntityParam);
        }

        public MonthEntity GetSingleById(MonthEntity MonthEntityParam)
        {
            MonthEntity o = GetMonthFromMonthDB(
                _MonthDB.GetSingleMonthDB(MonthEntityParam));

            return o;
        }

        public List<MonthEntity> GetAll()
        {
            List<MonthEntity> lst = new List<MonthEntity>();
            //string key = "Month_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MonthEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMonthCollectionFromMonthDBList(
                _MonthDB.GetAllMonthDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<MonthEntity> GetAllIsActive()
        {
            List<MonthEntity> lst = new List<MonthEntity>();
            //string key = "Month_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MonthEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetMonthCollectionFromMonthDBList(
                _MonthDB.GetAllMonthIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<MonthEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "Month_List_Page_" + currentPage ;
            //string countKey = "Month_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<MonthEntity> lst = new List<MonthEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<MonthEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetMonthCollectionFromMonthDBList(
                _MonthDB.GetPageMonthDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private MonthEntity GetMonthFromMonthDB(MonthEntity o)
        {
            if (o == null)
                return null;
            MonthEntity ret = new MonthEntity(o.MonthId, o.CalendarTypeId, o.SeasonId, o.MonthTitle, o.MonthNo,
                o.IsActive, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<MonthEntity> GetMonthCollectionFromMonthDBList(List<MonthEntity> lst)
        {
            List<MonthEntity> RetLst = new List<MonthEntity>();
            foreach (MonthEntity o in lst)
            {
                RetLst.Add(GetMonthFromMonthDB(o));
            }
            return RetLst;

        }

        public List<MonthEntity> GetMonthCollectionByCalendarType(MonthEntity monthEntityParam)
        {
            return GetMonthCollectionFromMonthDBList(_MonthDB.GetMonthDBCollectionByCalendarTypeDB(monthEntityParam));
        }

        public List<MonthEntity> GetMonthCollectionBySeason(MonthEntity monthEntityParam)
        {
            return GetMonthCollectionFromMonthDBList(_MonthDB.GetMonthDBCollectionBySeasonDB(monthEntityParam));
        }


        public List<MonthEntity> GetMonthCollectionBySeasonNo(SeasonEntity seasonEntity)
        {
            return GetMonthCollectionFromMonthDBList(_MonthDB.GetMonthCollectionBySeasonNoDB(seasonEntity));
        }
    }

}