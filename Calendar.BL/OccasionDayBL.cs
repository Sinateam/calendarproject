﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت روز>
    /// </summary>

    public class OccasionDayBL : DeskTopObj
    {
        private readonly OccasionDayDB _OccasionDayDB;

        public OccasionDayBL()
        {
            _OccasionDayDB = new OccasionDayDB();
        }

        public void Add(OccasionDayEntity OccasionDayEntityParam, out int OccasionDayId)
        {
            _OccasionDayDB.AddOccasionDayDB(OccasionDayEntityParam, out OccasionDayId);
        }

        public void Update(OccasionDayEntity OccasionDayEntityParam)
        {
            _OccasionDayDB.UpdateOccasionDayDB(OccasionDayEntityParam);
        }

        public void Delete(OccasionDayEntity OccasionDayEntityParam)
        {
            _OccasionDayDB.DeleteOccasionDayDB(OccasionDayEntityParam);
        }

        public OccasionDayEntity GetSingleById(OccasionDayEntity OccasionDayEntityParam)
        {
            OccasionDayEntity o = GetOccasionDayFromOccasionDayDB(
                _OccasionDayDB.GetSingleOccasionDayDB(OccasionDayEntityParam));

            return o;
        }

        public List<OccasionDayEntity> GetAll()
        {
            List<OccasionDayEntity> lst = new List<OccasionDayEntity>();
            //string key = "OccasionDay_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionDayEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetOccasionDayCollectionFromOccasionDayDBList(
                _OccasionDayDB.GetAllOccasionDayDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<OccasionDayEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "OccasionDay_List_Page_" + currentPage ;
            //string countKey = "OccasionDay_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<OccasionDayEntity> lst = new List<OccasionDayEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<OccasionDayEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetOccasionDayCollectionFromOccasionDayDBList(
                _OccasionDayDB.GetPageOccasionDayDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private OccasionDayEntity GetOccasionDayFromOccasionDayDB(OccasionDayEntity o)
        {
            if (o == null)
                return null;
            OccasionDayEntity ret = new OccasionDayEntity(o.OccasionDayId, o.OccasionDayTitle, o.OccasionDayTypeId,
                o.IsOff, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<OccasionDayEntity> GetOccasionDayCollectionFromOccasionDayDBList(List<OccasionDayEntity> lst)
        {
            List<OccasionDayEntity> RetLst = new List<OccasionDayEntity>();
            foreach (OccasionDayEntity o in lst)
            {
                RetLst.Add(GetOccasionDayFromOccasionDayDB(o));
            }
            return RetLst;

        }



        public  List<OccasionDayEntity> GetOccasionDayCollectionByOccasionDayType(OccasionDayEntity occasionDayEntityParam)
        {
            return
                GetOccasionDayCollectionFromOccasionDayDBList(
                    _OccasionDayDB.GetOccasionDayDBCollectionByOccasionDayTypeDB(occasionDayEntityParam));
        }



    }

}