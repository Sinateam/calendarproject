﻿using System.Collections.Generic;
using System.Data;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/02>
    /// Description: < تقویم مشتری>
    /// </summary>


	public class CustomCalendarBL : DeskTopObj
	{	
	  	 private readonly CustomCalendarDB _CustomCalendarDB;					
			
		public CustomCalendarBL()
		{
			_CustomCalendarDB = new CustomCalendarDB();
		}			
	
		public  void Add(CustomCalendarEntity  CustomCalendarEntityParam,out int CustomCalendarId)
		{ 
			_CustomCalendarDB.AddCustomCalendarDB(CustomCalendarEntityParam,out CustomCalendarId);			
		}

		public  void Update(CustomCalendarEntity  CustomCalendarEntityParam)
		{
			_CustomCalendarDB.UpdateCustomCalendarDB(CustomCalendarEntityParam);		
		}
        public void UpdateIsHoliday(CustomCalendarEntity CustomCalendarEntityParam)
        {
            _CustomCalendarDB.UpdateCustomCalendarIsHolidayDB(CustomCalendarEntityParam);
        }
		public  void Delete(CustomCalendarEntity  CustomCalendarEntityParam)
		{
			 _CustomCalendarDB.DeleteCustomCalendarDB(CustomCalendarEntityParam);			
		}

		public  CustomCalendarEntity GetSingleById(CustomCalendarEntity  CustomCalendarEntityParam)
		{
			CustomCalendarEntity o = GetCustomCalendarFromCustomCalendarDB(
			_CustomCalendarDB.GetSingleCustomCalendarDB(CustomCalendarEntityParam));
			
			return o;
		}

		public  List<CustomCalendarEntity> GetAll()
		{
			List<CustomCalendarEntity> lst = new List<CustomCalendarEntity>();
			//string key = "CustomCalendar_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CustomCalendarEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetCustomCalendarCollectionFromCustomCalendarDBList(
				_CustomCalendarDB.GetAllCustomCalendarDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<CustomCalendarEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "CustomCalendar_List_Page_" + currentPage ;
			//string countKey = "CustomCalendar_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<CustomCalendarEntity> lst = new List<CustomCalendarEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CustomCalendarEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetCustomCalendarCollectionFromCustomCalendarDBList(
				_CustomCalendarDB.GetPageCustomCalendarDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  CustomCalendarEntity GetCustomCalendarFromCustomCalendarDB(CustomCalendarEntity o)
		{
	if(o == null)
                return null;
			CustomCalendarEntity ret = new CustomCalendarEntity(o.CustomCalendarId ,o.OrganizationPhysicalChartId ,o.WorkDayTypeId ,o.MiladiDate ,o.YearPersianId ,o.YearArabicId ,o.YearEnglishId ,o.MonthPersianId ,o.MonthArabicId ,o.MonthEnglishId ,o.WeekDayId ,o.PersianDayNo ,o.ArabicDayNo ,o.EnglishDayNo ,o.Description ,o.IsHoliday ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<CustomCalendarEntity> GetCustomCalendarCollectionFromCustomCalendarDBList( List<CustomCalendarEntity> lst)
		{
			List<CustomCalendarEntity> RetLst = new List<CustomCalendarEntity>();
			foreach(CustomCalendarEntity o in lst)
			{
				RetLst.Add(GetCustomCalendarFromCustomCalendarDB(o));
			}
			return RetLst;
			
		}


        public List<CustomCalendarEntity> GetCustomCalendarCollectionByMonth(CustomCalendarEntity CustomCalendarEntityParam)
{
    return GetCustomCalendarCollectionFromCustomCalendarDBList(_CustomCalendarDB.GetCustomCalendarDBCollectionByMonthDB(CustomCalendarEntityParam));
}



public List<CustomCalendarEntity> GetCustomCalendarCollectionByWeekDay(CustomCalendarEntity CustomCalendarEntityParam)
{
    return GetCustomCalendarCollectionFromCustomCalendarDBList(_CustomCalendarDB.GetCustomCalendarDBCollectionByWeekDayDB(CustomCalendarEntityParam));
}

public List<CustomCalendarEntity> GetCustomCalendarCollectionByYear(CustomCalendarEntity CustomCalendarEntityParam)
{
    return GetCustomCalendarCollectionFromCustomCalendarDBList(_CustomCalendarDB.GetCustomCalendarDBCollectionByYearDB(CustomCalendarEntityParam));
}



public List<CustomCalendarEntity> GetCustomCalendarCollectionByOrganizationPhysicalChart(CustomCalendarEntity CustomCalendarEntityParam)
{
    return GetCustomCalendarCollectionFromCustomCalendarDBList(_CustomCalendarDB.GetCustomCalendarDBCollectionByOrganizationPhysicalChartDB(CustomCalendarEntityParam));
}

public List<CustomCalendarEntity> GetCustomCalendarCollectionByWorkDayType(CustomCalendarEntity CustomCalendarEntityParam)
{
    return GetCustomCalendarCollectionFromCustomCalendarDBList(_CustomCalendarDB.GetCustomCalendarDBCollectionByWorkDayTypeDB(CustomCalendarEntityParam));
}

        public DataSet DayCalanderCustomGetAll(GenerationDateEntity generationDateEntity)
        {
          return  _CustomCalendarDB.DayCalanderCustomGetAllDB(generationDateEntity);
        }
	}

}