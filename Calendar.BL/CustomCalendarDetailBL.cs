﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/05>
    /// Description: <مناسبت تقویم مشتری>
    /// </summary>

	public class CustomCalendarDetailBL : DeskTopObj
	{	
	  	 private readonly CustomCalendarDetailDB _CustomCalendarDetailDB;					
			
		public CustomCalendarDetailBL()
		{
			_CustomCalendarDetailDB = new CustomCalendarDetailDB();
		}			
	
		public  void Add(CustomCalendarDetailEntity  CustomCalendarDetailEntityParam,out int CustomCalendarDetailId)
		{ 
			_CustomCalendarDetailDB.AddCustomCalendarDetailDB(CustomCalendarDetailEntityParam,out CustomCalendarDetailId);			
		}

		public  void Update(CustomCalendarDetailEntity  CustomCalendarDetailEntityParam)
		{
			_CustomCalendarDetailDB.UpdateCustomCalendarDetailDB(CustomCalendarDetailEntityParam);		
		}

		public  void Delete(CustomCalendarDetailEntity  CustomCalendarDetailEntityParam)
		{
			 _CustomCalendarDetailDB.DeleteCustomCalendarDetailDB(CustomCalendarDetailEntityParam);			
		}

		public  CustomCalendarDetailEntity GetSingleById(CustomCalendarDetailEntity  CustomCalendarDetailEntityParam)
		{
			CustomCalendarDetailEntity o = GetCustomCalendarDetailFromCustomCalendarDetailDB(
			_CustomCalendarDetailDB.GetSingleCustomCalendarDetailDB(CustomCalendarDetailEntityParam));
			
			return o;
		}

		public  List<CustomCalendarDetailEntity> GetAll()
		{
			List<CustomCalendarDetailEntity> lst = new List<CustomCalendarDetailEntity>();
			//string key = "CustomCalendarDetail_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CustomCalendarDetailEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetCustomCalendarDetailCollectionFromCustomCalendarDetailDBList(
				_CustomCalendarDetailDB.GetAllCustomCalendarDetailDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<CustomCalendarDetailEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "CustomCalendarDetail_List_Page_" + currentPage ;
			//string countKey = "CustomCalendarDetail_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<CustomCalendarDetailEntity> lst = new List<CustomCalendarDetailEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<CustomCalendarDetailEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetCustomCalendarDetailCollectionFromCustomCalendarDetailDBList(
				_CustomCalendarDetailDB.GetPageCustomCalendarDetailDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  CustomCalendarDetailEntity GetCustomCalendarDetailFromCustomCalendarDetailDB(CustomCalendarDetailEntity o)
		{
	if(o == null)
                return null;
			CustomCalendarDetailEntity ret = new CustomCalendarDetailEntity(o.CustomCalendarDetailId ,o.CustomCalendarId ,o.CalendarTypeId ,o.OccasionDayId ,o.IsActive ,o.CreationDate ,o.ModificationDate,o.OccasionDayTypeId );
			return ret;
		}
		
		private  List<CustomCalendarDetailEntity> GetCustomCalendarDetailCollectionFromCustomCalendarDetailDBList( List<CustomCalendarDetailEntity> lst)
		{
			List<CustomCalendarDetailEntity> RetLst = new List<CustomCalendarDetailEntity>();
			foreach(CustomCalendarDetailEntity o in lst)
			{
				RetLst.Add(GetCustomCalendarDetailFromCustomCalendarDetailDB(o));
			}
			return RetLst;
			
		}


        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailCollectionByCustomCalendar(
            CustomCalendarDetailEntity customCalendarDetailEntityParam)
        {
            return
                GetCustomCalendarDetailCollectionFromCustomCalendarDetailDBList(
                    _CustomCalendarDetailDB.GetCustomCalendarDetailDBCollectionByCustomCalendarDB(
                        customCalendarDetailEntityParam));
        }

        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailCollectionByOccasionDay(
            CustomCalendarDetailEntity customCalendarDetailEntityParam)
        {
            return
                GetCustomCalendarDetailCollectionFromCustomCalendarDetailDBList(
                    _CustomCalendarDetailDB.GetCustomCalendarDetailDBCollectionByOccasionDayDB(
                        customCalendarDetailEntityParam));
        }




	}

}