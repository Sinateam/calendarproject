﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/27>
    /// Description: <سال>
    /// </summary>

//-------------------------------------------------------


	/// <summary>
	/// 
	/// </summary>
	public class YearBL : DeskTopObj
	{	
	  	 private readonly YearDB _YearDB;					
			
		public YearBL()
		{
			_YearDB = new YearDB();
		}			
	
		public  void Add(YearEntity  YearEntityParam,out int YearId)
		{ 
			_YearDB.AddYearDB(YearEntityParam,out YearId);			
		}

		public  void Update(YearEntity  YearEntityParam)
		{
			_YearDB.UpdateYearDB(YearEntityParam);		
		}

		public  void Delete(YearEntity  YearEntityParam)
		{
			 _YearDB.DeleteYearDB(YearEntityParam);			
		}

		public  YearEntity GetSingleById(YearEntity  YearEntityParam)
		{
			YearEntity o = GetYearFromYearDB(
			_YearDB.GetSingleYearDB(YearEntityParam));
			
			return o;
		}

		public  List<YearEntity> GetAll()
		{
			List<YearEntity> lst = new List<YearEntity>();
			//string key = "Year_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<YearEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetYearCollectionFromYearDBList(
				_YearDB.GetAllYearDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<YearEntity> GetAllIsActive()
        {
            List<YearEntity> lst = new List<YearEntity>();
            //string key = "Year_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<YearEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetYearCollectionFromYearDBList(
            _YearDB.GetAllYearIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<YearEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Year_List_Page_" + currentPage ;
			//string countKey = "Year_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<YearEntity> lst = new List<YearEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<YearEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetYearCollectionFromYearDBList(
				_YearDB.GetPageYearDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  YearEntity GetYearFromYearDB(YearEntity o)
		{
	if(o == null)
                return null;
			YearEntity ret = new YearEntity(o.YearId ,o.CalendarTypeId ,o.YearTitle ,o.YearNo ,o.YearStartDate ,o.YearDaysNo ,o.Description ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<YearEntity> GetYearCollectionFromYearDBList( List<YearEntity> lst)
		{
			List<YearEntity> RetLst = new List<YearEntity>();
			foreach(YearEntity o in lst)
			{
				RetLst.Add(GetYearFromYearDB(o));
			}
			return RetLst;
			
		}



        public List<YearEntity> GetYearCollectionByCalendarType(YearEntity yearEntityParam)
{
    return GetYearCollectionFromYearDBList(_YearDB.GetYearDBCollectionByCalendarTypeDB(yearEntityParam));
}




}

}