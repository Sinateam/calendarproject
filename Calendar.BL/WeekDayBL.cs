﻿using System.Collections.Generic;
using Intranet.Configuration;
using Intranet.DesktopModules.CalendarProject.Calendar.DAL;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <اطلاعات هفته>
    /// </summary>

	public class WeekDayBL : DeskTopObj
	{	
	  	 private readonly WeekDayDB _WeekDayDB;					
			
		public WeekDayBL()
		{
			_WeekDayDB = new WeekDayDB();
		}			
	
		public  void Add(WeekDayEntity  WeekDayEntityParam,out int WeekDayId)
		{ 
			_WeekDayDB.AddWeekDayDB(WeekDayEntityParam,out WeekDayId);			
		}

		public  void Update(WeekDayEntity  WeekDayEntityParam)
		{
			_WeekDayDB.UpdateWeekDayDB(WeekDayEntityParam);		
		}

		public  void Delete(WeekDayEntity  WeekDayEntityParam)
		{
			 _WeekDayDB.DeleteWeekDayDB(WeekDayEntityParam);			
		}

		public  WeekDayEntity GetSingleById(WeekDayEntity  WeekDayEntityParam)
		{
			WeekDayEntity o = GetWeekDayFromWeekDayDB(
			_WeekDayDB.GetSingleWeekDayDB(WeekDayEntityParam));
			
			return o;
		}

		public  List<WeekDayEntity> GetAll()
		{
			List<WeekDayEntity> lst = new List<WeekDayEntity>();
			//string key = "WeekDay_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WeekDayEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetWeekDayCollectionFromWeekDayDBList(
				_WeekDayDB.GetAllWeekDayDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<WeekDayEntity> GetAllIsActive()
        {
            List<WeekDayEntity> lst = new List<WeekDayEntity>();
            //string key = "WeekDay_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<WeekDayEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetWeekDayCollectionFromWeekDayDBList(
            _WeekDayDB.GetAllWeekDayIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		
		public  List<WeekDayEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "WeekDay_List_Page_" + currentPage ;
			//string countKey = "WeekDay_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<WeekDayEntity> lst = new List<WeekDayEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<WeekDayEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetWeekDayCollectionFromWeekDayDBList(
				_WeekDayDB.GetPageWeekDayDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  WeekDayEntity GetWeekDayFromWeekDayDB(WeekDayEntity o)
		{
	if(o == null)
                return null;
			WeekDayEntity ret = new WeekDayEntity(o.WeekDayId ,o.WeekDayPersianTitle ,o.WeekDayEnglishTitle ,o.WeekDayArabicTitle ,o.WeekDayNo ,o.BackGroundColor ,o.IsWeekEnd ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<WeekDayEntity> GetWeekDayCollectionFromWeekDayDBList( List<WeekDayEntity> lst)
		{
			List<WeekDayEntity> RetLst = new List<WeekDayEntity>();
			foreach(WeekDayEntity o in lst)
			{
				RetLst.Add(GetWeekDayFromWeekDayDB(o));
			}
			return RetLst;
			
		} 
				
		
	}
}

