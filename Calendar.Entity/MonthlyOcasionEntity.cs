﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت در ماه>
    /// </summary>
    public class MonthlyOcasionEntity
    {
                 #region Properties :

        public int MonthlyOcasionId { get; set; }
        public int MonthId { get; set; }

        public int OccasionDayId { get; set; }

        public int? SatrtDayNo { get; set; }

        public int? EndDayNo { get; set; }

        public bool? IsFirstDayOfMonth { get; set; }

        public bool? IsLastDayOfMonth { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        #endregion

        #region Constrauctors :

        public MonthlyOcasionEntity()
        {
        }

        public MonthlyOcasionEntity(int _MonthlyOcasionId, int MonthId, int OccasionDayId, int? SatrtDayNo, int? EndDayNo, bool? IsFirstDayOfMonth, bool? IsLastDayOfMonth, string CreationDate, string ModificationDate)
        {
            MonthlyOcasionId = _MonthlyOcasionId;
            this.MonthId = MonthId;
            this.OccasionDayId = OccasionDayId;
            this.SatrtDayNo = SatrtDayNo;
            this.EndDayNo = EndDayNo;
            this.IsFirstDayOfMonth = IsFirstDayOfMonth;
            this.IsLastDayOfMonth = IsLastDayOfMonth;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
        }

        #endregion
    }
}