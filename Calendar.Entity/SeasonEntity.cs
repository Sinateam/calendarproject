﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{/// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <فصل>
    /// </summary>
    public class SeasonEntity
    {
                 #region Properties :

        public int SeasonId { get; set; }
        public string SeasonPersianTitle { get; set; }

        public string SeasonEnglishTitle { get; set; }

        public byte? SeasonNo { get; set; }

        public string BackGroundColor { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public SeasonEntity()
        {
        }

        public SeasonEntity(int _SeasonId, string SeasonPersianTitle, string SeasonEnglishTitle, byte? SeasonNo, string BackGroundColor, bool IsActive, string CreationDate, string ModificationDate)
        {
            SeasonId = _SeasonId;
            this.SeasonPersianTitle = SeasonPersianTitle;
            this.SeasonEnglishTitle = SeasonEnglishTitle;
            this.SeasonNo = SeasonNo;
            this.BackGroundColor = BackGroundColor;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}