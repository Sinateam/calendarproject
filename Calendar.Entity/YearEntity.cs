﻿using System;

namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/27>
    /// Description: <سال>
    /// </summary>
    public class YearEntity
    {
              
        #region Properties :

        public int YearId { get; set; }
        public int CalendarTypeId { get; set; }

        public string YearTitle { get; set; }

        public int YearNo { get; set; }

        public DateTime YearStartDate { get; set; }

        public int YearDaysNo { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public YearEntity()
        {
        }

        public YearEntity(int _YearId, int CalendarTypeId, string YearTitle, int YearNo, DateTime YearStartDate, int YearDaysNo, string Description, bool IsActive, string CreationDate, string ModificationDate)
        {
            YearId = _YearId;
            this.CalendarTypeId = CalendarTypeId;
            this.YearTitle = YearTitle;
            this.YearNo = YearNo;
            this.YearStartDate = YearStartDate;
            this.YearDaysNo = YearDaysNo;
            this.Description = Description;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}