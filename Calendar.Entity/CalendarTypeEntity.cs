﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/26>
    /// Description: <نوع تقویم>
    /// </summary>
    public class CalendarTypeEntity
    {
                 #region Properties :

        public int CalendarTypeId { get; set; }
        public string CalendarTypePersianTitle { get; set; }

        public string CalendarTypeEnglishTitle { get; set; }

        public string CalendarTypeArabicTitle { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public CalendarTypeEntity()
        {
        }

        public CalendarTypeEntity(int _CalendarTypeId, string CalendarTypePersianTitle, string CalendarTypeEnglishTitle, string CalendarTypeArabicTitle, bool IsActive, string CreationDate, string ModificationDate)
        {
            CalendarTypeId = _CalendarTypeId;
            this.CalendarTypePersianTitle = CalendarTypePersianTitle;
            this.CalendarTypeEnglishTitle = CalendarTypeEnglishTitle;
            this.CalendarTypeArabicTitle = CalendarTypeArabicTitle;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}