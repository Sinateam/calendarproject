﻿using System;

namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    public class GenerationDateEntity
    {
        #region Property:
        public int YearId { get; set; }

        public int CalendarTypeId { get; set; }
        public Guid OrganizationPhysicalChartId { get; set; }
        public int MonthNo { get; set; }    
        #endregion

        #region Constrauctors :

        public GenerationDateEntity()
        {
            
        }

        public GenerationDateEntity(int YearId, int CalendarTypeId, Guid OrganizationPhysicalChartId,int MonthNo)
        {
            this.YearId = YearId;
            this.CalendarTypeId = CalendarTypeId;
            this.OrganizationPhysicalChartId = OrganizationPhysicalChartId;
            this.MonthNo = MonthNo;
        }

        #endregion

    }
    

}