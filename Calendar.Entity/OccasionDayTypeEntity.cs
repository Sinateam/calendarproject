﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <نوع مناسبت>
    /// </summary>
    public class OccasionDayTypeEntity
    {
                 #region Properties :

        public int OccasionDayTypeId { get; set; }
        public string OccasionDayTypeTitle { get; set; }

        public byte Priority { get; set; }

        public string BackGroundColor { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public OccasionDayTypeEntity()
        {
        }

        public OccasionDayTypeEntity(int _OccasionDayTypeId, string OccasionDayTypeTitle, byte Priority, string BackGroundColor, string Description, bool IsActive, string CreationDate, string ModificationDate)
        {
            OccasionDayTypeId = _OccasionDayTypeId;
            this.OccasionDayTypeTitle = OccasionDayTypeTitle;
            this.Priority = Priority;
            this.BackGroundColor = BackGroundColor;
            this.Description = Description;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}