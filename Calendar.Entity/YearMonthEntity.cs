﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <مشخصات ماه های سال>
    /// </summary>
    public class YearMonthEntity
    {
                   #region Properties :

            public int YearMonthId { get; set; }
            public int YearId { get; set; }

            public int MonthId { get; set; }

            public string DayNo { get; set; }

            public bool IsActive { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }



            #endregion

            #region Constrauctors :

            public YearMonthEntity()
            {
            }

            public YearMonthEntity(int _YearMonthId, int YearId, int MonthId, string DayNo, bool IsActive, string CreationDate, string ModificationDate)
            {
                YearMonthId = _YearMonthId;
                this.YearId = YearId;
                this.MonthId = MonthId;
                this.DayNo = DayNo;
                this.IsActive = IsActive;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;

            }

            #endregion  
    }
}