﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/05>
    /// Description: <مناسبت تقویم مشتری>
    /// </summary>
    public class CustomCalendarDetailEntity
    {
         
        #region Properties :

        public int CustomCalendarDetailId { get; set; }
        public int CustomCalendarId { get; set; }

        public int CalendarTypeId { get; set; }

        public int OccasionDayId { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public int OccasionDayTypeId { get; set; }

        #endregion

        #region Constrauctors :

        public CustomCalendarDetailEntity()
        {
        }

        public CustomCalendarDetailEntity(int _CustomCalendarDetailId, int CustomCalendarId, int CalendarTypeId, int OccasionDayId, bool IsActive, string CreationDate, string ModificationDate, int OccasionDayTypeId)
        {
            CustomCalendarDetailId = _CustomCalendarDetailId;
            this.CustomCalendarId = CustomCalendarId;
            this.CalendarTypeId = CalendarTypeId;
            this.OccasionDayId = OccasionDayId;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.OccasionDayTypeId = OccasionDayTypeId;

        }

        #endregion
    }
}