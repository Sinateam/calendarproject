﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <ماه>
    /// </summary>
    public class MonthEntity
    {
               #region Properties :

        public int MonthId { get; set; }
        public int CalendarTypeId { get; set; }

        public int? SeasonId { get; set; }

        public string MonthTitle { get; set; }

        public byte MonthNo { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public MonthEntity()
        {
        }

        public MonthEntity(int _MonthId, int CalendarTypeId, int? SeasonId, string MonthTitle, byte MonthNo, bool IsActive, string CreationDate, string ModificationDate)
        {
            MonthId = _MonthId;
            this.CalendarTypeId = CalendarTypeId;
            this.SeasonId = SeasonId;
            this.MonthTitle = MonthTitle;
            this.MonthNo = MonthNo;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}