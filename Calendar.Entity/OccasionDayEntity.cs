﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت روز>
    /// </summary>
    public class OccasionDayEntity
    {
       #region Properties :

        public int OccasionDayId { get; set; }
        public string OccasionDayTitle { get; set; }

        public int? OccasionDayTypeId { get; set; }

        public bool IsOff { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        #endregion

        #region Constrauctors :

        public OccasionDayEntity()
        {
        }

        public OccasionDayEntity(int _OccasionDayId, string OccasionDayTitle, int? OccasionDayTypeId, bool IsOff, string CreationDate, string ModificationDate)
        {
            OccasionDayId = _OccasionDayId;
            this.OccasionDayTitle = OccasionDayTitle;
            this.OccasionDayTypeId = OccasionDayTypeId;
            this.IsOff = IsOff;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion

    }
}