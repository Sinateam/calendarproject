﻿namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <اطلاعات هفته>
    /// </summary>
    public class WeekDayEntity
    {
              #region Properties :

        public int WeekDayId { get; set; }
        public string WeekDayPersianTitle { get; set; }

        public string WeekDayEnglishTitle { get; set; }

        public string WeekDayArabicTitle { get; set; }

        public byte WeekDayNo { get; set; }

        public string BackGroundColor { get; set; }

        public bool IsWeekEnd { get; set; }

        public bool IsActive { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public WeekDayEntity()
        {
        }

        public WeekDayEntity(int _WeekDayId, string WeekDayPersianTitle, string WeekDayEnglishTitle, string WeekDayArabicTitle, byte WeekDayNo, string BackGroundColor, bool IsWeekEnd, bool IsActive, string CreationDate, string ModificationDate)
        {
            WeekDayId = _WeekDayId;
            this.WeekDayPersianTitle = WeekDayPersianTitle;
            this.WeekDayEnglishTitle = WeekDayEnglishTitle;
            this.WeekDayArabicTitle = WeekDayArabicTitle;
            this.WeekDayNo = WeekDayNo;
            this.BackGroundColor = BackGroundColor;
            this.IsWeekEnd = IsWeekEnd;
            this.IsActive = IsActive;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion   
    }
}