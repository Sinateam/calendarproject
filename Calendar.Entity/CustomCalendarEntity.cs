﻿using System;

namespace Intranet.DesktopModules.CalendarProject.Calendar.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/02>
    /// Description: < تقویم مشتری>
    /// </summary>
    public class CustomCalendarEntity
    {
                 #region Properties :

        public int CustomCalendarId { get; set; }
        public Guid OrganizationPhysicalChartId { get; set; }

        public int WorkDayTypeId { get; set; }

        public DateTime MiladiDate { get; set; }

        public int YearPersianId { get; set; }

        public int? YearArabicId { get; set; }

        public int? YearEnglishId { get; set; }

        public int MonthPersianId { get; set; }

        public int? MonthArabicId { get; set; }

        public int? MonthEnglishId { get; set; }

        public int WeekDayId { get; set; }

        public int PersianDayNo { get; set; }

        public int? ArabicDayNo { get; set; }

        public int? EnglishDayNo { get; set; }

        public string Description { get; set; }

        public bool IsHoliday { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public CustomCalendarEntity()
        {
        }

        public CustomCalendarEntity(int _CustomCalendarId, Guid OrganizationPhysicalChartId, int WorkDayTypeId, DateTime MiladiDate, int YearPersianId, int? YearArabicId, int? YearEnglishId, int MonthPersianId, int? MonthArabicId, int? MonthEnglishId, int WeekDayId, int PersianDayNo, int? ArabicDayNo, int? EnglishDayNo, string Description, bool IsHoliday, string CreationDate, string ModificationDate)
        {
            CustomCalendarId = _CustomCalendarId;
            this.OrganizationPhysicalChartId = OrganizationPhysicalChartId;
            this.WorkDayTypeId = WorkDayTypeId;
            this.MiladiDate = MiladiDate;
            this.YearPersianId = YearPersianId;
            this.YearArabicId = YearArabicId;
            this.YearEnglishId = YearEnglishId;
            this.MonthPersianId = MonthPersianId;
            this.MonthArabicId = MonthArabicId;
            this.MonthEnglishId = MonthEnglishId;
            this.WeekDayId = WeekDayId;
            this.PersianDayNo = PersianDayNo;
            this.ArabicDayNo = ArabicDayNo;
            this.EnglishDayNo = EnglishDayNo;
            this.Description = Description;
            this.IsHoliday = IsHoliday;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}