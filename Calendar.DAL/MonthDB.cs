﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <ماه>
    /// </summary>
    
    public class MonthDB
    {  
        #region Methods :

        public void AddMonthDB(MonthEntity monthEntityParam, out int MonthId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MonthId", SqlDbType.Int, 4),
								 new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@SeasonId", SqlDbType.Int) ,
											  new SqlParameter("@MonthTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@MonthNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = monthEntityParam.CalendarTypeId;
            parameters[2].Value = (monthEntityParam.SeasonId==null?System.DBNull.Value:(object)monthEntityParam.SeasonId);
            parameters[3].Value = FarsiToArabic.ToArabic(monthEntityParam.MonthTitle.Trim());
            parameters[4].Value = monthEntityParam.MonthNo;
            parameters[5].Value = monthEntityParam.IsActive;            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_MonthAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MonthId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateMonthDB(MonthEntity monthEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MonthId", SqlDbType.Int, 4),
						 new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@SeasonId", SqlDbType.Int) ,
											  new SqlParameter("@MonthTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@MonthNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = monthEntityParam.MonthId;
            parameters[1].Value = monthEntityParam.CalendarTypeId;
            parameters[2].Value = (monthEntityParam.SeasonId == null ? System.DBNull.Value : (object)monthEntityParam.SeasonId);
            parameters[3].Value = FarsiToArabic.ToArabic(monthEntityParam.MonthTitle.Trim());
            parameters[4].Value = monthEntityParam.MonthNo;
            parameters[5].Value = monthEntityParam.IsActive;            

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_MonthUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMonthDB(MonthEntity MonthEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MonthId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MonthEntityParam.MonthId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_MonthDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MonthEntity GetSingleMonthDB(MonthEntity MonthEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@MonthId", DataTypes.integer, 4, MonthEntityParam.MonthId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_MonthGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMonthDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MonthEntity> GetAllMonthIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMonthDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_MonthIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<MonthEntity> GetAllMonthDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMonthDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_MonthGetAll", new IDataParameter[] { }));
        }

        public List<MonthEntity> GetPageMonthDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Month";
            parameters[5].Value = "MonthId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetMonthDBCollectionFromDataSet(ds, out count);
        }

        public MonthEntity GetMonthDBFromDataReader(IDataReader reader)
        {
            return new MonthEntity(int.Parse(reader["MonthId"].ToString()),
                                    int.Parse(reader["CalendarTypeId"].ToString()),
                                    Convert.IsDBNull(reader["SeasonId"]) ? null : (int?)reader["SeasonId"],                                    
                                    reader["MonthTitle"].ToString(),
                                    byte.Parse(reader["MonthNo"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<MonthEntity> GetMonthDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MonthEntity> lst = new List<MonthEntity>();
                while (reader.Read())
                    lst.Add(GetMonthDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MonthEntity> GetMonthDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMonthDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<MonthEntity> GetMonthDBCollectionByCalendarTypeDB(MonthEntity monthEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CalendarTypeId", SqlDbType.Int);
            parameter.Value = monthEntityParam.CalendarTypeId;
            return GetMonthDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_MonthGetByCalendarType", new[] { parameter }));
        }

        public  List<MonthEntity> GetMonthDBCollectionBySeasonDB(MonthEntity monthEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SeasonId", SqlDbType.Int);
            parameter.Value = monthEntityParam.SeasonId;
            return GetMonthDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_MonthGetBySeason", new[] { parameter }));
        }

        public List<MonthEntity> GetMonthCollectionBySeasonNoDB(SeasonEntity seasonEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SeasonNo", SqlDbType.TinyInt);
            parameter.Value = seasonEntity.SeasonNo;
            return GetMonthDBCollectionFromDataReader(
                  _intranetDB.RunProcedureReader
                  ("[Calendar].[p_MonthGetBySeasonNo]", new[] { parameter }));         
        }
        #endregion

        
    }
    
}