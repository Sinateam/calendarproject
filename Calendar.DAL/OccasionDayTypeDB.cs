﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <نوع مناسبت>
    /// </summary>

    public class OccasionDayTypeDB
    {
        #region Methods :

        public void AddOccasionDayTypeDB(OccasionDayTypeEntity occasionDayTypeEntityParam, out int OccasionDayTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OccasionDayTypeId", SqlDbType.Int, 4),
								 new SqlParameter("@OccasionDayTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionDayTypeEntityParam.OccasionDayTypeTitle.Trim());
            parameters[2].Value = occasionDayTypeEntityParam.Priority;
            parameters[3].Value = (occasionDayTypeEntityParam.BackGroundColor==""?System.DBNull.Value:(object)(FarsiToArabic.ToArabic(occasionDayTypeEntityParam.BackGroundColor.Trim())));
            parameters[4].Value = (occasionDayTypeEntityParam.Description== "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(occasionDayTypeEntityParam.Description.Trim())));
            parameters[5].Value = occasionDayTypeEntityParam.IsActive;
            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_OccasionDayTypeAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OccasionDayTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateOccasionDayTypeDB(OccasionDayTypeEntity occasionDayTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OccasionDayTypeId", SqlDbType.Int, 4),
						 new SqlParameter("@OccasionDayTypeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@Priority", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = occasionDayTypeEntityParam.OccasionDayTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(occasionDayTypeEntityParam.OccasionDayTypeTitle.Trim());
            parameters[2].Value = occasionDayTypeEntityParam.Priority;
            parameters[3].Value = (occasionDayTypeEntityParam.BackGroundColor == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(occasionDayTypeEntityParam.BackGroundColor.Trim())));
            parameters[4].Value = (occasionDayTypeEntityParam.Description == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(occasionDayTypeEntityParam.Description.Trim())));
            parameters[5].Value = occasionDayTypeEntityParam.IsActive;            

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_OccasionDayTypeUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOccasionDayTypeDB(OccasionDayTypeEntity OccasionDayTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OccasionDayTypeId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OccasionDayTypeEntityParam.OccasionDayTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_OccasionDayTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OccasionDayTypeEntity GetSingleOccasionDayTypeDB(OccasionDayTypeEntity OccasionDayTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@OccasionDayTypeId", DataTypes.integer, 4,OccasionDayTypeEntityParam.OccasionDayTypeId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_OccasionDayTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOccasionDayTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionDayTypeEntity> GetAllOccasionDayTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionDayTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_OccasionDayTypeGetAll", new IDataParameter[] { }));
        }

        public List<OccasionDayTypeEntity> GetAllOccasionDayTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionDayTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_OccasionDayTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<OccasionDayTypeEntity> GetPageOccasionDayTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_OccasionDayType";
            parameters[5].Value = "OccasionDayTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetOccasionDayTypeDBCollectionFromDataSet(ds, out count);
        }

        public OccasionDayTypeEntity GetOccasionDayTypeDBFromDataReader(IDataReader reader)
        {
            return new OccasionDayTypeEntity(int.Parse(reader["OccasionDayTypeId"].ToString()),
                                    reader["OccasionDayTypeTitle"].ToString(),
                                    byte.Parse(reader["Priority"].ToString()),
                                    reader["BackGroundColor"].ToString(),
                                    reader["Description"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<OccasionDayTypeEntity> GetOccasionDayTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OccasionDayTypeEntity> lst = new List<OccasionDayTypeEntity>();
                while (reader.Read())
                    lst.Add(GetOccasionDayTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionDayTypeEntity> GetOccasionDayTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOccasionDayTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }

}