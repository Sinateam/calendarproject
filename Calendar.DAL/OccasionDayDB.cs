﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت روز>
    /// </summary>
    public class OccasionDayDB
    {
        #region Methods :

        public void AddOccasionDayDB(OccasionDayEntity occasionDayEntityParam, out int OccasionDayId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@OccasionDayId", SqlDbType.Int, 4),
								 new SqlParameter("@OccasionDayTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@OccasionDayTypeId", SqlDbType.Int) ,
											  new SqlParameter("@IsOff", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = occasionDayEntityParam.OccasionDayTitle;
            parameters[2].Value = occasionDayEntityParam.OccasionDayTypeId;
            parameters[3].Value = occasionDayEntityParam.IsOff;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_OccasionDayAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            OccasionDayId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateOccasionDayDB(OccasionDayEntity occasionDayEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@OccasionDayId", SqlDbType.Int, 4),
						 new SqlParameter("@OccasionDayTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@OccasionDayTypeId", SqlDbType.Int) ,
											  new SqlParameter("@IsOff", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = occasionDayEntityParam.OccasionDayId;
            parameters[1].Value = occasionDayEntityParam.OccasionDayTitle;
            parameters[2].Value = occasionDayEntityParam.OccasionDayTypeId;
            parameters[3].Value = occasionDayEntityParam.IsOff;            

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_OccasionDayUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteOccasionDayDB(OccasionDayEntity OccasionDayEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@OccasionDayId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = OccasionDayEntityParam.OccasionDayId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_OccasionDayDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public OccasionDayEntity GetSingleOccasionDayDB(OccasionDayEntity OccasionDayEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@OccasionDayId", DataTypes.integer, 4,OccasionDayEntityParam.OccasionDayId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_OccasionDayGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetOccasionDayDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionDayEntity> GetAllOccasionDayDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetOccasionDayDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_OccasionDayGetAll", new IDataParameter[] { }));
        }

        public List<OccasionDayEntity> GetPageOccasionDayDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_OccasionDay";
            parameters[5].Value = "OccasionDayId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetOccasionDayDBCollectionFromDataSet(ds, out count);
        }

        public OccasionDayEntity GetOccasionDayDBFromDataReader(IDataReader reader)
        {
            return new OccasionDayEntity(int.Parse(reader["OccasionDayId"].ToString()),
                                    reader["OccasionDayTitle"].ToString(),
                                    int.Parse(reader["OccasionDayTypeId"].ToString()),
                                    bool.Parse(reader["IsOff"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<OccasionDayEntity> GetOccasionDayDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<OccasionDayEntity> lst = new List<OccasionDayEntity>();
                while (reader.Read())
                    lst.Add(GetOccasionDayDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<OccasionDayEntity> GetOccasionDayDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetOccasionDayDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<OccasionDayEntity> GetOccasionDayDBCollectionByOccasionDayTypeDB(OccasionDayEntity occasionDayEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayTypeId", SqlDbType.Int);
            parameter.Value = occasionDayEntityParam.OccasionDayTypeId;
            return GetOccasionDayDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_OccasionDayGetByOccasionDayType", new[] { parameter }));
        }
        #endregion
    }
}