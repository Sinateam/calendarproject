﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/05>
    /// Description: <مناسبت تقویم مشتری>
    /// </summary>

    public class CustomCalendarDetailDB
    {


        #region Methods :

        public void AddCustomCalendarDetailDB(CustomCalendarDetailEntity customCalendarDetailEntityParam, out int CustomCalendarDetailId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@CustomCalendarDetailId", SqlDbType.Int, 4),
								 new SqlParameter("@CustomCalendarId", SqlDbType.Int) ,
											  new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = customCalendarDetailEntityParam.CustomCalendarId;
            parameters[2].Value = customCalendarDetailEntityParam.CalendarTypeId;
            parameters[3].Value = customCalendarDetailEntityParam.OccasionDayId;
            parameters[4].Value = customCalendarDetailEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CustomCalendarDetailAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CustomCalendarDetailId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateCustomCalendarDetailDB(CustomCalendarDetailEntity customCalendarDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CustomCalendarDetailId", SqlDbType.Int, 4),
						 new SqlParameter("@CustomCalendarId", SqlDbType.Int) ,
											  new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = customCalendarDetailEntityParam.CustomCalendarDetailId;
            parameters[1].Value = customCalendarDetailEntityParam.CustomCalendarId;
            parameters[2].Value = customCalendarDetailEntityParam.CalendarTypeId;
            parameters[3].Value = customCalendarDetailEntityParam.OccasionDayId;
            parameters[4].Value = customCalendarDetailEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_CustomCalendarDetailUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteCustomCalendarDetailDB(CustomCalendarDetailEntity CustomCalendarDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@CustomCalendarDetailId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = CustomCalendarDetailEntityParam.CustomCalendarDetailId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CustomCalendarDetailDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CustomCalendarDetailEntity GetSingleCustomCalendarDetailDB(CustomCalendarDetailEntity CustomCalendarDetailEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@CustomCalendarDetailId", DataTypes.integer, 4, 										CustomCalendarDetailEntityParam.CustomCalendarDetailId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_CustomCalendarDetailGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCustomCalendarDetailDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CustomCalendarDetailEntity> GetAllCustomCalendarDetailDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCustomCalendarDetailDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_CustomCalendarDetailGetAll", new IDataParameter[] { }));
        }

        public List<CustomCalendarDetailEntity> GetPageCustomCalendarDetailDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_CustomCalendarDetail";
            parameters[5].Value = "CustomCalendarDetailId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetCustomCalendarDetailDBCollectionFromDataSet(ds, out count);
        }

        public CustomCalendarDetailEntity GetCustomCalendarDetailDBFromDataReader(IDataReader reader)
        {
            return new CustomCalendarDetailEntity(int.Parse(reader["CustomCalendarDetailId"].ToString()),
                                    int.Parse(reader["CustomCalendarId"].ToString()),
                                    int.Parse(reader["CalendarTypeId"].ToString()),
                                    int.Parse(reader["OccasionDayId"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    int.Parse(reader["OccasionDayTypeId"].ToString()));
        }

        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CustomCalendarDetailEntity> lst = new List<CustomCalendarDetailEntity>();
                while (reader.Read())
                    lst.Add(GetCustomCalendarDetailDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCustomCalendarDetailDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailDBCollectionByCustomCalendarDB(CustomCalendarDetailEntity customCalendarDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CustomCalendarId", SqlDbType.Int);
            parameter.Value = customCalendarDetailEntityParam.CustomCalendarId;
            return GetCustomCalendarDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarDetailGetByCustomCalendar", new[] { parameter }));
        }

        public List<CustomCalendarDetailEntity> GetCustomCalendarDetailDBCollectionByOccasionDayDB(CustomCalendarDetailEntity customCalendarDetailEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayId", SqlDbType.Int);
            parameter.Value = customCalendarDetailEntityParam.OccasionDayId;
            return GetCustomCalendarDetailDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarDetailGetByOccasionDay", new[] { parameter }));
        }


        #endregion



    }

    
}