﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/31>
    /// Description: <تقویم>
    /// </summary>

    //-----------------------------------------------------
    #region Class "GeneralCalendarDB"

    public class GeneralCalendarDB
    {


        #region Methods :

        public void AddGeneralCalendarDB(GeneralCalendarEntity generalCalendarEntityParam, out int generalCalendarId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@GeneralCalendarId", SqlDbType.Int, 4),
								 new SqlParameter("@MiladiDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearPersianId", SqlDbType.Int) ,
											  new SqlParameter("@YearArabicId", SqlDbType.Int) ,
											  new SqlParameter("@YearEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@MonthPersianId", SqlDbType.Int) ,
											  new SqlParameter("@MonthArabicId", SqlDbType.Int) ,
											  new SqlParameter("@MonthEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@WeekDayId", SqlDbType.Int) ,
											  new SqlParameter("@PersianDayNo", SqlDbType.Int) ,
											  new SqlParameter("@ArabicDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EnglishDayNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsHoliday", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = generalCalendarEntityParam.MiladiDate;
            parameters[2].Value = generalCalendarEntityParam.YearPersianId;
            parameters[3].Value = generalCalendarEntityParam.YearArabicId;
            parameters[4].Value = generalCalendarEntityParam.YearEnglishId;
            parameters[5].Value = generalCalendarEntityParam.MonthPersianId;
            parameters[6].Value = generalCalendarEntityParam.MonthArabicId;
            parameters[7].Value = generalCalendarEntityParam.MonthEnglishId;
            parameters[8].Value = generalCalendarEntityParam.WeekDayId;
            parameters[9].Value = generalCalendarEntityParam.PersianDayNo;
            parameters[10].Value = generalCalendarEntityParam.ArabicDayNo;
            parameters[11].Value = generalCalendarEntityParam.EnglishDayNo;
            parameters[12].Value = generalCalendarEntityParam.Description;
            parameters[13].Value = generalCalendarEntityParam.IsHoliday;            

            parameters[14].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_GeneralCalendarAdd", parameters);
            var messageError = parameters[14].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            generalCalendarId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateGeneralCalendarDB(GeneralCalendarEntity generalCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@GeneralCalendarId", SqlDbType.Int, 4),
						 new SqlParameter("@MiladiDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearPersianId", SqlDbType.Int) ,
											  new SqlParameter("@YearArabicId", SqlDbType.Int) ,
											  new SqlParameter("@YearEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@MonthPersianId", SqlDbType.Int) ,
											  new SqlParameter("@MonthArabicId", SqlDbType.Int) ,
											  new SqlParameter("@MonthEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@WeekDayId", SqlDbType.Int) ,
											  new SqlParameter("@PersianDayNo", SqlDbType.Int) ,
											  new SqlParameter("@ArabicDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EnglishDayNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsHoliday", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = generalCalendarEntityParam.GeneralCalendarId;
            parameters[1].Value = generalCalendarEntityParam.MiladiDate;
            parameters[2].Value = generalCalendarEntityParam.YearPersianId;
            parameters[3].Value = generalCalendarEntityParam.YearArabicId;
            parameters[4].Value = generalCalendarEntityParam.YearEnglishId;
            parameters[5].Value = generalCalendarEntityParam.MonthPersianId;
            parameters[6].Value = generalCalendarEntityParam.MonthArabicId;
            parameters[7].Value = generalCalendarEntityParam.MonthEnglishId;
            parameters[8].Value = generalCalendarEntityParam.WeekDayId;
            parameters[9].Value = generalCalendarEntityParam.PersianDayNo;
            parameters[10].Value = generalCalendarEntityParam.ArabicDayNo;
            parameters[11].Value = generalCalendarEntityParam.EnglishDayNo;
            parameters[12].Value = generalCalendarEntityParam.Description;
            parameters[13].Value = generalCalendarEntityParam.IsHoliday;            

            parameters[14].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("p_GeneralCalendarUpdate", parameters);
            var messageError = parameters[14].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteGeneralCalendarDB(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@GeneralCalendarId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = GeneralCalendarEntityParam.GeneralCalendarId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("p_GeneralCalendarDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public GeneralCalendarEntity GetSingleGeneralCalendarDB(GeneralCalendarEntity GeneralCalendarEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@GeneralCalendarId", DataTypes.integer, 4, GeneralCalendarEntityParam.GeneralCalendarId)
						      };

                reader = _intranetDB.RunProcedureReader("p_GeneralCalendarGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetGeneralCalendarDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GeneralCalendarEntity> GetAllGeneralCalendarDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetGeneralCalendarDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("p_GeneralCalendarGetAll", new IDataParameter[] { }));
        }

        public List<GeneralCalendarEntity> GetPageGeneralCalendarDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_GeneralCalendar";
            parameters[5].Value = "GeneralCalendarId";
            DataSet ds = _intranetDB.RunProcedureDS("p_TablesGetPage", parameters);
            return GetGeneralCalendarDBCollectionFromDataSet(ds, out count);
        }

        public GeneralCalendarEntity GetGeneralCalendarDBFromDataReader(IDataReader reader)
        {
            return new GeneralCalendarEntity(int.Parse(reader["GeneralCalendarId"].ToString()),
                                    DateTime.Parse(reader["MiladiDate"].ToString()),
                                    int.Parse(reader["YearPersianId"].ToString()),
                                    int.Parse(reader["YearArabicId"].ToString()),
                                    int.Parse(reader["YearEnglishId"].ToString()),
                                    int.Parse(reader["MonthPersianId"].ToString()),
                                    int.Parse(reader["MonthArabicId"].ToString()),
                                    int.Parse(reader["MonthEnglishId"].ToString()),
                                    int.Parse(reader["WeekDayId"].ToString()),
                                    int.Parse(reader["PersianDayNo"].ToString()),
                                    int.Parse(reader["ArabicDayNo"].ToString()),
                                    int.Parse(reader["EnglishDayNo"].ToString()),
                                    reader["Description"].ToString(),
                                    bool.Parse(reader["IsHoliday"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<GeneralCalendarEntity> GetGeneralCalendarDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<GeneralCalendarEntity> lst = new List<GeneralCalendarEntity>();
                while (reader.Read())
                    lst.Add(GetGeneralCalendarDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<GeneralCalendarEntity> GetGeneralCalendarDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetGeneralCalendarDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<GeneralCalendarEntity> GetGeneralCalendarDBCollectionByMonthDB(GeneralCalendarEntity generalCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MonthPersianId", SqlDbType.Int);
            parameter.Value = generalCalendarEntityParam.MonthPersianId;
            return GetGeneralCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("p_GeneralCalendarGetByMonth", new[] { parameter }));
        }



        public List<GeneralCalendarEntity> GetGeneralCalendarDBCollectionByWeekDayDB(GeneralCalendarEntity generalCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WeekDayId", SqlDbType.Int);
            parameter.Value = generalCalendarEntityParam.WeekDayId;
            return GetGeneralCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("p_GeneralCalendarGetByWeekDay", new[] { parameter }));
        }

        public List<GeneralCalendarEntity> GetGeneralCalendarDBCollectionByYearDB(GeneralCalendarEntity generalCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@YearPersianId", SqlDbType.Int);
            parameter.Value = generalCalendarEntityParam.YearPersianId;
            return GetGeneralCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("p_GeneralCalendarGetByYear", new[] { parameter }));
        }

        

        #endregion

        public void GenerationDateDB(GenerationDateEntity generationDateEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@YearId", SqlDbType.Int, 4),																  
                                 new SqlParameter("@CalendarTypeId", SqlDbType.Int, 4),																  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Value = generationDateEntity.YearId;
            parameters[1].Value = generationDateEntity.CalendarTypeId;

            parameters[2].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_GenerationDate", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));            
        }

        public void GenerationDateDeleteDB(GenerationDateEntity generationDateEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@YearId", SqlDbType.Int, 4),																  
                                 new SqlParameter("@CalendarTypeId", SqlDbType.Int, 4),																  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Value = generationDateEntity.YearId;
            parameters[1].Value = generationDateEntity.CalendarTypeId;

            parameters[2].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Calendar].[p_GenerationDateDelete]", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));  
        }

        public void GenerationDateCopyDB(GenerationDateEntity generationDateEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@YearId", SqlDbType.Int, 4),																		                                   														  
                                 new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier, 4),		
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Value = generationDateEntity.YearId;            
            parameters[1].Value = generationDateEntity.OrganizationPhysicalChartId;
            parameters[2].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[Calendar].[p_GenerationDateCopy]", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError)); 
        }
    }

    #endregion
}