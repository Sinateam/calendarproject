﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/07/02>
    /// Description: < تقویم مشتری>
    /// </summary>

    //-----------------------------------------------------
    #region Class "CustomCalendarDB"

    public class CustomCalendarDB
    {



        #region Methods :

        public void AddCustomCalendarDB(CustomCalendarEntity customCalendarEntityParam, out int CustomCalendarId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@CustomCalendarId", SqlDbType.Int, 4),
								 new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@WorkDayTypeId", SqlDbType.Int) ,
											  new SqlParameter("@MiladiDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearPersianId", SqlDbType.Int) ,
											  new SqlParameter("@YearArabicId", SqlDbType.Int) ,
											  new SqlParameter("@YearEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@MonthPersianId", SqlDbType.Int) ,
											  new SqlParameter("@MonthArabicId", SqlDbType.Int) ,
											  new SqlParameter("@MonthEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@WeekDayId", SqlDbType.Int) ,
											  new SqlParameter("@PersianDayNo", SqlDbType.Int) ,
											  new SqlParameter("@ArabicDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EnglishDayNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsHoliday", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (customCalendarEntityParam.OrganizationPhysicalChartId);
            parameters[2].Value = customCalendarEntityParam.WorkDayTypeId;
            parameters[3].Value = customCalendarEntityParam.MiladiDate;
            parameters[4].Value = customCalendarEntityParam.YearPersianId;
            parameters[5].Value = (customCalendarEntityParam.YearArabicId==null?System.DBNull.Value:(object)customCalendarEntityParam.YearArabicId);
            parameters[6].Value = (customCalendarEntityParam.YearEnglishId==null?System.DBNull.Value:(object)customCalendarEntityParam.YearEnglishId);
            parameters[7].Value = (customCalendarEntityParam.MonthPersianId);
            parameters[8].Value = (customCalendarEntityParam.MonthArabicId==null?System.DBNull.Value:(object)customCalendarEntityParam.MonthArabicId);
            parameters[9].Value = (customCalendarEntityParam.MonthEnglishId==null?System.DBNull.Value:(object)customCalendarEntityParam.MonthEnglishId);
            parameters[10].Value = customCalendarEntityParam.WeekDayId;
            parameters[11].Value = customCalendarEntityParam.PersianDayNo;
            parameters[12].Value = (customCalendarEntityParam.ArabicDayNo==null?System.DBNull.Value:(object)customCalendarEntityParam.ArabicDayNo);
            parameters[13].Value = (customCalendarEntityParam.EnglishDayNo==null?System.DBNull.Value:(object)customCalendarEntityParam.EnglishDayNo);
            parameters[14].Value = customCalendarEntityParam.Description;
            parameters[15].Value = customCalendarEntityParam.IsHoliday;            

            parameters[16].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CustomCalendarAdd", parameters);
            var messageError = parameters[16].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CustomCalendarId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateCustomCalendarDB(CustomCalendarEntity customCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CustomCalendarId", SqlDbType.Int, 4),
						 new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@WorkDayTypeId", SqlDbType.Int) ,
											  new SqlParameter("@MiladiDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearPersianId", SqlDbType.Int) ,
											  new SqlParameter("@YearArabicId", SqlDbType.Int) ,
											  new SqlParameter("@YearEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@MonthPersianId", SqlDbType.Int) ,
											  new SqlParameter("@MonthArabicId", SqlDbType.Int) ,
											  new SqlParameter("@MonthEnglishId", SqlDbType.Int) ,
											  new SqlParameter("@WeekDayId", SqlDbType.Int) ,
											  new SqlParameter("@PersianDayNo", SqlDbType.Int) ,
											  new SqlParameter("@ArabicDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EnglishDayNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsHoliday", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = customCalendarEntityParam.CustomCalendarId;
            parameters[1].Value = (customCalendarEntityParam.OrganizationPhysicalChartId);
            parameters[2].Value = customCalendarEntityParam.WorkDayTypeId;
            parameters[3].Value = customCalendarEntityParam.MiladiDate;
            parameters[4].Value = customCalendarEntityParam.YearPersianId;
            parameters[5].Value = (customCalendarEntityParam.YearArabicId == null ? System.DBNull.Value : (object)customCalendarEntityParam.YearArabicId);
            parameters[6].Value = (customCalendarEntityParam.YearEnglishId == null ? System.DBNull.Value : (object)customCalendarEntityParam.YearEnglishId);
            parameters[7].Value = (customCalendarEntityParam.MonthPersianId);
            parameters[8].Value = (customCalendarEntityParam.MonthArabicId == null ? System.DBNull.Value : (object)customCalendarEntityParam.MonthArabicId);
            parameters[9].Value = (customCalendarEntityParam.MonthEnglishId == null ? System.DBNull.Value : (object)customCalendarEntityParam.MonthEnglishId);
            parameters[10].Value = customCalendarEntityParam.WeekDayId;
            parameters[11].Value = customCalendarEntityParam.PersianDayNo;
            parameters[12].Value = (customCalendarEntityParam.ArabicDayNo == null ? System.DBNull.Value : (object)customCalendarEntityParam.ArabicDayNo);
            parameters[13].Value = (customCalendarEntityParam.EnglishDayNo == null ? System.DBNull.Value : (object)customCalendarEntityParam.EnglishDayNo);
            parameters[14].Value = customCalendarEntityParam.Description;
            parameters[15].Value = customCalendarEntityParam.IsHoliday; 

            parameters[16].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_CustomCalendarUpdate", parameters);
            var messageError = parameters[16].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void UpdateCustomCalendarIsHolidayDB(CustomCalendarEntity customCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CustomCalendarId", SqlDbType.Int, 4),
											  new SqlParameter("@IsHoliday", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = customCalendarEntityParam.CustomCalendarId;
            parameters[1].Value = customCalendarEntityParam.IsHoliday;

            parameters[2].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_CustomCalendarIsHolidayUpdate", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }
        public void DeleteCustomCalendarDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@CustomCalendarId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = CustomCalendarEntityParam.CustomCalendarId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CustomCalendarDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CustomCalendarEntity GetSingleCustomCalendarDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@CustomCalendarId", DataTypes.integer, 4,CustomCalendarEntityParam.CustomCalendarId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCustomCalendarDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CustomCalendarEntity> GetAllCustomCalendarDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCustomCalendarDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_CustomCalendarGetAll", new IDataParameter[] { }));
        }

        public List<CustomCalendarEntity> GetPageCustomCalendarDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_CustomCalendar";
            parameters[5].Value = "CustomCalendarId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetCustomCalendarDBCollectionFromDataSet(ds, out count);
        }

        public CustomCalendarEntity GetCustomCalendarDBFromDataReader(IDataReader reader)
        {
            return new CustomCalendarEntity(int.Parse(reader["CustomCalendarId"].ToString()),
                                    Guid.Parse(reader["OrganizationPhysicalChartId"].ToString()),
                                    int.Parse(reader["WorkDayTypeId"].ToString()),
                                    DateTime.Parse(reader["MiladiDate"].ToString()),
                                    int.Parse(reader["YearPersianId"].ToString()),
                                    Convert.IsDBNull(reader["YearArabicId"]) ? null : (int?)reader["YearArabicId"],
                                    Convert.IsDBNull(reader["YearEnglishId"]) ? null : (int?)reader["YearEnglishId"],                                    
                                    int.Parse(reader["MonthPersianId"].ToString()),
                                    Convert.IsDBNull(reader["MonthArabicId"]) ? null : (int?)reader["MonthArabicId"],
                                    Convert.IsDBNull(reader["MonthEnglishId"]) ? null : (int?)reader["MonthEnglishId"],                                      
                                    int.Parse(reader["WeekDayId"].ToString()),
                                    int.Parse(reader["PersianDayNo"].ToString()),
                                                     Convert.IsDBNull(reader["ArabicDayNo"]) ? null : (int?)reader["ArabicDayNo"],
                                    Convert.IsDBNull(reader["EnglishDayNo"]) ? null : (int?)reader["EnglishDayNo"],  
       
                                    reader["Description"].ToString(),
                                    bool.Parse(reader["IsHoliday"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CustomCalendarEntity> lst = new List<CustomCalendarEntity>();
                while (reader.Read())
                    lst.Add(GetCustomCalendarDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCustomCalendarDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionByMonthDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MonthPersianId", SqlDbType.Int);
            parameter.Value = CustomCalendarEntityParam.MonthPersianId;
            return GetCustomCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetByMonth", new[] { parameter }));
        }



        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionByWeekDayDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WeekDayId", SqlDbType.Int);
            parameter.Value = CustomCalendarEntityParam.WeekDayId;
            return GetCustomCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetByWeekDay", new[] { parameter }));
        }

        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionByYearDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@YearPersianId", SqlDbType.Int);
            parameter.Value = CustomCalendarEntityParam.YearPersianId;
            return GetCustomCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetByYear", new[] { parameter }));
        }



        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionByOrganizationPhysicalChartDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OrganizationPhysicalChartId", SqlDbType.Int);
            parameter.Value = CustomCalendarEntityParam.OrganizationPhysicalChartId;
            return GetCustomCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetByOrganizationPhysicalChart", new[] { parameter }));
        }

        public List<CustomCalendarEntity> GetCustomCalendarDBCollectionByWorkDayTypeDB(CustomCalendarEntity CustomCalendarEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@WorkDayTypeId", SqlDbType.Int);
            parameter.Value = CustomCalendarEntityParam.WorkDayTypeId;
            return GetCustomCalendarDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_CustomCalendarGetByWorkDayType", new[] { parameter }));
        }


        #endregion

        public DataSet DayCalanderCustomGetAllDB(GenerationDateEntity generationDateEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@YearId", SqlDbType.Int, 4),
                                            new SqlParameter("@MonthNo", SqlDbType.Int, 4),
                                            new SqlParameter("@OrganizationPhysicalChartId",SqlDbType.UniqueIdentifier), 
				     		  };

            parameters[0].Value = generationDateEntity.YearId;
            parameters[1].Value = generationDateEntity.MonthNo;
            parameters[2].Value = generationDateEntity.OrganizationPhysicalChartId;


           return _intranetDB.RunProcedureDS("calendar.[p_DayCalanderCustomGetAll]", parameters);
            
        }
    }

    #endregion
}