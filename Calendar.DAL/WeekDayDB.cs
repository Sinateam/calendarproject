﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <اطلاعات هفته>
    /// </summary>

    public class WeekDayDB
    {
        #region Methods :

        public void AddWeekDayDB(WeekDayEntity weekDayEntityParam, out int WeekDayId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@WeekDayId", SqlDbType.Int, 4),
								 new SqlParameter("@WeekDayPersianTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayArabicTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsWeekEnd", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayPersianTitle.Trim());
            parameters[2].Value = (weekDayEntityParam.WeekDayEnglishTitle == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayEnglishTitle)));
            parameters[3].Value = (weekDayEntityParam.WeekDayArabicTitle==""?System.DBNull.Value:(object)(FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayArabicTitle)));
            parameters[4].Value = weekDayEntityParam.WeekDayNo;
            parameters[5].Value = (weekDayEntityParam.BackGroundColor==""?System.DBNull.Value:(object)(FarsiToArabic.ToArabic(weekDayEntityParam.BackGroundColor)));
            parameters[6].Value = weekDayEntityParam.IsWeekEnd;
            parameters[7].Value = weekDayEntityParam.IsActive;            
            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_WeekDayAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            WeekDayId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateWeekDayDB(WeekDayEntity weekDayEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@WeekDayId", SqlDbType.Int, 4),
						 new SqlParameter("@WeekDayPersianTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayArabicTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@WeekDayNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsWeekEnd", SqlDbType.Bit) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = weekDayEntityParam.WeekDayId;
            parameters[1].Value = FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayPersianTitle.Trim());
            parameters[2].Value = (weekDayEntityParam.WeekDayEnglishTitle == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayEnglishTitle)));
            parameters[3].Value = (weekDayEntityParam.WeekDayArabicTitle == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(weekDayEntityParam.WeekDayArabicTitle)));
            parameters[4].Value = weekDayEntityParam.WeekDayNo;
            parameters[5].Value = (weekDayEntityParam.BackGroundColor == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(weekDayEntityParam.BackGroundColor)));
            parameters[6].Value = weekDayEntityParam.IsWeekEnd;
            parameters[7].Value = weekDayEntityParam.IsActive;
            parameters[8].Direction = ParameterDirection.Output;
            

            _intranetDB.RunProcedure("Calendar.p_WeekDayUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteWeekDayDB(WeekDayEntity WeekDayEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@WeekDayId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = WeekDayEntityParam.WeekDayId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_WeekDayDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public WeekDayEntity GetSingleWeekDayDB(WeekDayEntity WeekDayEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@WeekDayId", DataTypes.integer, 4,WeekDayEntityParam.WeekDayId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_WeekDayGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetWeekDayDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WeekDayEntity> GetAllWeekDayDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWeekDayDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_WeekDayGetAll", new IDataParameter[] { }));
        }

        public List<WeekDayEntity> GetAllWeekDayIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetWeekDayDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_WeekDayIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<WeekDayEntity> GetPageWeekDayDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_WeekDay";
            parameters[5].Value = "WeekDayId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetWeekDayDBCollectionFromDataSet(ds, out count);
        }

        public WeekDayEntity GetWeekDayDBFromDataReader(IDataReader reader)
        {
            return new WeekDayEntity(int.Parse(reader["WeekDayId"].ToString()),
                                    reader["WeekDayPersianTitle"].ToString(),
                                    reader["WeekDayEnglishTitle"].ToString(),
                                    reader["WeekDayArabicTitle"].ToString(),
                                    byte.Parse(reader["WeekDayNo"].ToString()),
                                    reader["BackGroundColor"].ToString(),
                                    bool.Parse(reader["IsWeekEnd"].ToString()),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<WeekDayEntity> GetWeekDayDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<WeekDayEntity> lst = new List<WeekDayEntity>();
                while (reader.Read())
                    lst.Add(GetWeekDayDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<WeekDayEntity> GetWeekDayDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetWeekDayDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion

    }
}