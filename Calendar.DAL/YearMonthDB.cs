﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <مشخصات ماه های سال>
    /// </summary>


        public class YearMonthDB
        {
            #region Methods :

            public void AddYearMonthDB(YearMonthEntity yearMonthEntityParam, out int YearMonthId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@YearMonthId", SqlDbType.Int, 4),
								 new SqlParameter("@YearId", SqlDbType.Int) ,
											  new SqlParameter("@MonthId", SqlDbType.Int) ,
											  new SqlParameter("@DayNo", SqlDbType.NVarChar,10) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = yearMonthEntityParam.YearId;
                parameters[2].Value = yearMonthEntityParam.MonthId;
                parameters[3].Value = yearMonthEntityParam.DayNo.Trim();
                parameters[4].Value = yearMonthEntityParam.IsActive;                

                parameters[5].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Calendar.p_YearMonthAdd", parameters);
                var messageError = parameters[5].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                YearMonthId = int.Parse(parameters[0].Value.ToString());
            }

            public void UpdateYearMonthDB(YearMonthEntity yearMonthEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@YearMonthId", SqlDbType.Int, 4),
						 new SqlParameter("@YearId", SqlDbType.Int) ,
											  new SqlParameter("@MonthId", SqlDbType.Int) ,
											  new SqlParameter("@DayNo", SqlDbType.NVarChar,10) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = yearMonthEntityParam.YearMonthId;
                parameters[1].Value = yearMonthEntityParam.YearId;
                parameters[2].Value = yearMonthEntityParam.MonthId;
                parameters[3].Value = yearMonthEntityParam.DayNo.Trim();
                parameters[4].Value = yearMonthEntityParam.IsActive;                

                parameters[5].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("Calendar.p_YearMonthUpdate", parameters);
                var messageError = parameters[5].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteYearMonthDB(YearMonthEntity YearMonthEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@YearMonthId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = YearMonthEntityParam.YearMonthId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("Calendar.p_YearMonthDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public YearMonthEntity GetSingleYearMonthDB(YearMonthEntity YearMonthEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@YearMonthId", DataTypes.integer, 4,YearMonthEntityParam.YearMonthId)
						      };

                    reader = _intranetDB.RunProcedureReader("Calendar.p_YearMonthGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetYearMonthDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<YearMonthEntity> GetAllYearMonthDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetYearMonthDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("Calendar.p_YearMonthGetAll", new IDataParameter[] { }));
            }

            public List<YearMonthEntity> GetPageYearMonthDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_YearMonth";
                parameters[5].Value = "YearMonthId";
                DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
                return GetYearMonthDBCollectionFromDataSet(ds, out count);
            }

            public YearMonthEntity GetYearMonthDBFromDataReader(IDataReader reader)
            {
                return new YearMonthEntity(int.Parse(reader["YearMonthId"].ToString()),
                                        int.Parse(reader["YearId"].ToString()),
                                        int.Parse(reader["MonthId"].ToString()),
                                        reader["DayNo"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString());
            }

            public List<YearMonthEntity> GetYearMonthDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<YearMonthEntity> lst = new List<YearMonthEntity>();
                    while (reader.Read())
                        lst.Add(GetYearMonthDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<YearMonthEntity> GetYearMonthDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetYearMonthDBCollectionFromDataReader(ds.CreateDataReader());
            }

            public  List<YearMonthEntity> GetYearMonthDBCollectionByMonthDB(YearMonthEntity yearMonthEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@MonthId", SqlDbType.Int);
                parameter.Value = yearMonthEntityParam.MonthId;
                return GetYearMonthDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_YearMonthGetByMonth", new[] { parameter }));
            }

            public  List<YearMonthEntity> GetYearMonthDBCollectionByYearDB(YearMonthEntity yearMonthEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter parameter = new SqlParameter("@YearId", SqlDbType.Int);
                parameter.Value = yearMonthEntityParam.YearId;
                return GetYearMonthDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_YearMonthGetByYear", new[] { parameter }));
            }


            #endregion



        }


    }
