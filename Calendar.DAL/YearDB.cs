﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/27>
    /// Description: <سال>
    /// </summary>




    public class YearDB
    {


        #region Methods :

        public void AddYearDB(YearEntity yearEntityParam, out int YearId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@YearId", SqlDbType.Int, 4),
								 new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@YearTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@YearNo", SqlDbType.Int) ,
											  new SqlParameter("@YearStartDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearDaysNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = yearEntityParam.CalendarTypeId;
            parameters[2].Value = FarsiToArabic.ToArabic(yearEntityParam.YearTitle.Trim());
            parameters[3].Value = yearEntityParam.YearNo;
            parameters[4].Value = yearEntityParam.YearStartDate;
            parameters[5].Value = yearEntityParam.YearDaysNo;
            parameters[6].Value =(yearEntityParam.Description==""?System.DBNull.Value:(object)FarsiToArabic.ToArabic(yearEntityParam.Description.Trim()));
            parameters[7].Value = yearEntityParam.IsActive;            

            parameters[8].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_YearAdd", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            YearId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateYearDB(YearEntity yearEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@YearId", SqlDbType.Int, 4),
						 new SqlParameter("@CalendarTypeId", SqlDbType.Int) ,
											  new SqlParameter("@YearTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@YearNo", SqlDbType.Int) ,
											  new SqlParameter("@YearStartDate", SqlDbType.DateTime) ,
											  new SqlParameter("@YearDaysNo", SqlDbType.Int) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = yearEntityParam.YearId;
            parameters[1].Value = yearEntityParam.CalendarTypeId;
            parameters[2].Value = FarsiToArabic.ToArabic(yearEntityParam.YearTitle.Trim());
            parameters[3].Value = yearEntityParam.YearNo;
            parameters[4].Value = yearEntityParam.YearStartDate;
            parameters[5].Value = yearEntityParam.YearDaysNo;
            parameters[6].Value = (yearEntityParam.Description == "" ? System.DBNull.Value : (object)FarsiToArabic.ToArabic(yearEntityParam.Description.Trim()));
            parameters[7].Value = yearEntityParam.IsActive;

            parameters[8].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_YearUpdate", parameters);
            var messageError = parameters[8].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteYearDB(YearEntity YearEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@YearId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = YearEntityParam.YearId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_YearDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public YearEntity GetSingleYearDB(YearEntity YearEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@YearId", DataTypes.integer, 4, 										YearEntityParam.YearId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_YearGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetYearDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<YearEntity> GetAllYearDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetYearDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_YearGetAll", new IDataParameter[] { }));
        }
        public List<YearEntity> GetAllYearIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetYearDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_YearIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<YearEntity> GetPageYearDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Year";
            parameters[5].Value = "YearId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetYearDBCollectionFromDataSet(ds, out count);
        }

        public YearEntity GetYearDBFromDataReader(IDataReader reader)
        {
            return new YearEntity(int.Parse(reader["YearId"].ToString()),
                                    int.Parse(reader["CalendarTypeId"].ToString()),
                                    reader["YearTitle"].ToString(),
                                    int.Parse(reader["YearNo"].ToString()),
                                    DateTime.Parse(reader["YearStartDate"].ToString()),
                                    int.Parse(reader["YearDaysNo"].ToString()),
                                    reader["Description"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<YearEntity> GetYearDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<YearEntity> lst = new List<YearEntity>();
                while (reader.Read())
                    lst.Add(GetYearDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<YearEntity> GetYearDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetYearDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<YearEntity> GetYearDBCollectionByCalendarTypeDB(YearEntity yearEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@CalendarTypeId", SqlDbType.Int);
            parameter.Value = yearEntityParam.CalendarTypeId;
            return GetYearDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_YearGetByCalendarType", new[] { parameter }));
        }


        #endregion



    }


}