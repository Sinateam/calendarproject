﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/29>
    /// Description: <فصل>
    /// </summary>

    //-----------------------------------------------------
    #region Class "SeasonDB"

    public class SeasonDB
    {



        #region Methods :

        public void AddSeasonDB(SeasonEntity seasonEntityParam, out int SeasonId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SeasonId", SqlDbType.Int, 4),
								 new SqlParameter("@SeasonPersianTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SeasonEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SeasonNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(seasonEntityParam.SeasonPersianTitle.Trim());
            parameters[2].Value = (seasonEntityParam.SeasonEnglishTitle==""?System.DBNull.Value:(object)(FarsiToArabic.ToArabic(seasonEntityParam.SeasonEnglishTitle.Trim())));
            parameters[3].Value = (seasonEntityParam.SeasonNo==null?System.DBNull.Value:(object)seasonEntityParam.SeasonNo);
            parameters[4].Value = (seasonEntityParam.BackGroundColor==""?System.DBNull.Value:(object)seasonEntityParam.BackGroundColor);
            parameters[5].Value = seasonEntityParam.IsActive;            

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_SeasonAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SeasonId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateSeasonDB(SeasonEntity seasonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SeasonId", SqlDbType.Int, 4),
						 new SqlParameter("@SeasonPersianTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SeasonEnglishTitle", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@SeasonNo", SqlDbType.TinyInt) ,
											  new SqlParameter("@BackGroundColor", SqlDbType.NVarChar,50) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = seasonEntityParam.SeasonId;
            parameters[1].Value = FarsiToArabic.ToArabic(seasonEntityParam.SeasonPersianTitle.Trim());
            parameters[2].Value = (seasonEntityParam.SeasonEnglishTitle == "" ? System.DBNull.Value : (object)(FarsiToArabic.ToArabic(seasonEntityParam.SeasonEnglishTitle.Trim())));
            parameters[3].Value = (seasonEntityParam.SeasonNo == null ? System.DBNull.Value : (object)seasonEntityParam.SeasonNo);
            parameters[4].Value = (seasonEntityParam.BackGroundColor == "" ? System.DBNull.Value : (object)seasonEntityParam.BackGroundColor);
            parameters[5].Value = seasonEntityParam.IsActive;    
            

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_SeasonUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSeasonDB(SeasonEntity SeasonEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SeasonId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SeasonEntityParam.SeasonId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_SeasonDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SeasonEntity GetSingleSeasonDB(SeasonEntity SeasonEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@SeasonId", DataTypes.integer, 4,SeasonEntityParam.SeasonId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_SeasonGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSeasonDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }


        public SeasonEntity GetSingleBySeasonNoDB(SeasonEntity seasonEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@SeasonNo", DataTypes.integer, 4,seasonEntityParam.SeasonNo)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_SeasonBySeasonNoGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSeasonDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }
        public List<SeasonEntity> GetAllSeasonDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSeasonDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_SeasonGetAll", new IDataParameter[] { }));
        }
        public List<SeasonEntity> GetAllSeasonIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSeasonDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_SeasonIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<SeasonEntity> GetPageSeasonDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Season";
            parameters[5].Value = "SeasonId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetSeasonDBCollectionFromDataSet(ds, out count);
        }

        public SeasonEntity GetSeasonDBFromDataReader(IDataReader reader)
        {
            return new SeasonEntity(int.Parse(reader["SeasonId"].ToString()),
                                    reader["SeasonPersianTitle"].ToString(),
                                    reader["SeasonEnglishTitle"].ToString(),
                                     Convert.IsDBNull(reader["SeasonNo"]) ? null : (byte?)reader["SeasonNo"],                                     
                                    reader["BackGroundColor"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<SeasonEntity> GetSeasonDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SeasonEntity> lst = new List<SeasonEntity>();
                while (reader.Read())
                    lst.Add(GetSeasonDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SeasonEntity> GetSeasonDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSeasonDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion


    }

    #endregion
}