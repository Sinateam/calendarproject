﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/26>
    /// Description: <نوع تقویم>
    /// </summary>


    public class CalendarTypeDB
    {
        #region Methods :

        public void AddCalendarTypeDB(CalendarTypeEntity calendarTypeEntityParam, out int CalendarTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@CalendarTypeId", SqlDbType.Int, 4),
								 new SqlParameter("@CalendarTypePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CalendarTypeEnglishTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CalendarTypeArabicTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypeEnglishTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypeArabicTitle.Trim());
            parameters[4].Value = calendarTypeEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CalendarTypeAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            CalendarTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateCalendarTypeDB(CalendarTypeEntity calendarTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@CalendarTypeId", SqlDbType.Int, 4),
						 new SqlParameter("@CalendarTypePersianTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CalendarTypeEnglishTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@CalendarTypeArabicTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = calendarTypeEntityParam.CalendarTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypePersianTitle.Trim());
            parameters[2].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypeEnglishTitle.Trim());
            parameters[3].Value = FarsiToArabic.ToArabic(calendarTypeEntityParam.CalendarTypeArabicTitle.Trim());
            parameters[4].Value = calendarTypeEntityParam.IsActive;            

            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_CalendarTypeUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteCalendarTypeDB(CalendarTypeEntity CalendarTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@CalendarTypeId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = CalendarTypeEntityParam.CalendarTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_CalendarTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public CalendarTypeEntity GetSingleCalendarTypeDB(CalendarTypeEntity CalendarTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@CalendarTypeId", DataTypes.integer, 4, 										CalendarTypeEntityParam.CalendarTypeId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_CalendarTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetCalendarTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CalendarTypeEntity> GetAllCalendarTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCalendarTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_CalendarTypeGetAll", new IDataParameter[] { }));
        }

        public List<CalendarTypeEntity> GetAllCalendarTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetCalendarTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_CalendarTypeIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<CalendarTypeEntity> GetPageCalendarTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_CalendarType";
            parameters[5].Value = "CalendarTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetCalendarTypeDBCollectionFromDataSet(ds, out count);
        }

        public CalendarTypeEntity GetCalendarTypeDBFromDataReader(IDataReader reader)
        {
            return new CalendarTypeEntity(int.Parse(reader["CalendarTypeId"].ToString()),
                                    reader["CalendarTypePersianTitle"].ToString(),
                                    reader["CalendarTypeEnglishTitle"].ToString(),
                                    reader["CalendarTypeArabicTitle"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<CalendarTypeEntity> GetCalendarTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<CalendarTypeEntity> lst = new List<CalendarTypeEntity>();
                while (reader.Read())
                    lst.Add(GetCalendarTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<CalendarTypeEntity> GetCalendarTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetCalendarTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    
}