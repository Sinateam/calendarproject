﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.CalendarProject.Calendar.Entity;

namespace Intranet.DesktopModules.CalendarProject.Calendar.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/06/30>
    /// Description: <مناسبت در ماه>
    /// </summary>

    public class MonthlyOcasionDB
    {
        #region Methods :

        public void AddMonthlyOcasionDB(MonthlyOcasionEntity monthlyOcasionEntityParam, out int MonthlyOcasionId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@MonthlyOcasionId", SqlDbType.Int, 4),
								 new SqlParameter("@MonthId", SqlDbType.Int) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@SatrtDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EndDayNo", SqlDbType.Int) ,
											  new SqlParameter("@IsFirstDayOfMonth", SqlDbType.Bit) ,
											  new SqlParameter("@IsLastDayOfMonth", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = monthlyOcasionEntityParam.MonthId;
            parameters[2].Value = monthlyOcasionEntityParam.OccasionDayId;
            parameters[3].Value = (monthlyOcasionEntityParam.SatrtDayNo==null?System.DBNull.Value:(object)monthlyOcasionEntityParam.SatrtDayNo);
            parameters[4].Value = (monthlyOcasionEntityParam.EndDayNo==null?System.DBNull.Value:(object)monthlyOcasionEntityParam.EndDayNo);
            parameters[5].Value = (monthlyOcasionEntityParam.IsFirstDayOfMonth==null?System.DBNull.Value:(object)monthlyOcasionEntityParam.IsFirstDayOfMonth);
            parameters[6].Value = (monthlyOcasionEntityParam.IsLastDayOfMonth==null?System.DBNull.Value:(object)monthlyOcasionEntityParam.IsLastDayOfMonth);

            parameters[7].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_MonthlyOcasionAdd", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            MonthlyOcasionId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateMonthlyOcasionDB(MonthlyOcasionEntity monthlyOcasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@MonthlyOcasionId", SqlDbType.Int, 4),
						 new SqlParameter("@MonthId", SqlDbType.Int) ,
											  new SqlParameter("@OccasionDayId", SqlDbType.Int) ,
											  new SqlParameter("@SatrtDayNo", SqlDbType.Int) ,
											  new SqlParameter("@EndDayNo", SqlDbType.Int) ,
											  new SqlParameter("@IsFirstDayOfMonth", SqlDbType.Bit) ,
											  new SqlParameter("@IsLastDayOfMonth", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = monthlyOcasionEntityParam.MonthlyOcasionId;
            parameters[1].Value = monthlyOcasionEntityParam.MonthId;
            parameters[2].Value = monthlyOcasionEntityParam.OccasionDayId;
            parameters[3].Value = (monthlyOcasionEntityParam.SatrtDayNo == null ? System.DBNull.Value : (object)monthlyOcasionEntityParam.SatrtDayNo);
            parameters[4].Value = (monthlyOcasionEntityParam.EndDayNo == null ? System.DBNull.Value : (object)monthlyOcasionEntityParam.EndDayNo);
            parameters[5].Value = (monthlyOcasionEntityParam.IsFirstDayOfMonth == null ? System.DBNull.Value : (object)monthlyOcasionEntityParam.IsFirstDayOfMonth);
            parameters[6].Value = (monthlyOcasionEntityParam.IsLastDayOfMonth == null ? System.DBNull.Value : (object)monthlyOcasionEntityParam.IsLastDayOfMonth);
            parameters[7].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("Calendar.p_MonthlyOcasionUpdate", parameters);
            var messageError = parameters[7].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteMonthlyOcasionDB(MonthlyOcasionEntity MonthlyOcasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@MonthlyOcasionId", SqlDbType.Int, 4),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = MonthlyOcasionEntityParam.MonthlyOcasionId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("Calendar.p_MonthlyOcasionDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public MonthlyOcasionEntity GetSingleMonthlyOcasionDB(MonthlyOcasionEntity MonthlyOcasionEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
		                                   		  	_intranetDB.GetParameter("@MonthlyOcasionId", DataTypes.integer, 4,MonthlyOcasionEntityParam.MonthlyOcasionId)
						      };

                reader = _intranetDB.RunProcedureReader("Calendar.p_MonthlyOcasionGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetMonthlyOcasionDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MonthlyOcasionEntity> GetAllMonthlyOcasionDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetMonthlyOcasionDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("Calendar.p_MonthlyOcasionGetAll", new IDataParameter[] { }));
        }

        public List<MonthlyOcasionEntity> GetPageMonthlyOcasionDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_MonthlyOcasion";
            parameters[5].Value = "MonthlyOcasionId";
            DataSet ds = _intranetDB.RunProcedureDS("Calendar.p_TablesGetPage", parameters);
            return GetMonthlyOcasionDBCollectionFromDataSet(ds, out count);
        }

        public MonthlyOcasionEntity GetMonthlyOcasionDBFromDataReader(IDataReader reader)
        {
            return new MonthlyOcasionEntity(int.Parse(reader["MonthlyOcasionId"].ToString()),
                                    int.Parse(reader["MonthId"].ToString()),
                                    int.Parse(reader["OccasionDayId"].ToString()),
                                    Convert.IsDBNull(reader["SatrtDayNo"]) ? null : (int?)reader["SatrtDayNo"],
                                    Convert.IsDBNull(reader["EndDayNo"]) ? null : (int?)reader["EndDayNo"],
                                    Convert.IsDBNull(reader["IsFirstDayOfMonth"]) ? null : (bool?)reader["IsFirstDayOfMonth"],
                                    Convert.IsDBNull(reader["IsLastDayOfMonth"]) ? null : (bool?)reader["IsLastDayOfMonth"],
                                         reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<MonthlyOcasionEntity> lst = new List<MonthlyOcasionEntity>();
                while (reader.Read())
                    lst.Add(GetMonthlyOcasionDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetMonthlyOcasionDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionDBCollectionByMonthDB(MonthlyOcasionEntity monthlyOcasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@MonthId", SqlDbType.Int);
            parameter.Value = monthlyOcasionEntityParam.MonthId;
            return GetMonthlyOcasionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_MonthlyOcasionGetByMonth", new[] { parameter }));
        }

        public List<MonthlyOcasionEntity> GetMonthlyOcasionDBCollectionByOccasionDayDB(MonthlyOcasionEntity monthlyOcasionEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@OccasionDayId", SqlDbType.Int);
            parameter.Value = monthlyOcasionEntityParam.OccasionDayId;
            return GetMonthlyOcasionDBCollectionFromDataReader(_intranetDB.RunProcedureReader("Calendar.p_MonthlyOcasionGetByOccasionDay", new[] { parameter }));
        }


        #endregion



    }

    
}